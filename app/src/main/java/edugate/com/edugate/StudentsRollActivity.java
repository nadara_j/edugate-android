package edugate.com.edugate;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import edugate.com.edugate.Adapter.StudentRollAdapter;
import edugate.com.edugate.Object.Student;


public class StudentsRollActivity extends AppCompatActivity
{

    private String          schoolId;
    private String          classId;
    private String          authToken;
    private String          accType;
    private String          subjectId;
    private String          eventStart;
    private String          eventEnd;
    private String          eventDate;



    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private ArrayList<Student> studentData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(this.getResources().getColor(R.color.colorAccent));
        }

        Intent intent   = getIntent();
        schoolId        = intent.getStringExtra("SCHOOL_ID");
        subjectId       = intent.getStringExtra("SUBJECT_ID");
        authToken       = intent.getStringExtra("AUTH_ID");
        accType         = intent.getStringExtra("ACC_TYPE");
        classId         = intent.getStringExtra("CLASS_ID");
        eventStart      = intent.getStringExtra("EVENT_START");
        eventEnd        = intent.getStringExtra("EVENT_END");
        eventDate       = intent.getStringExtra("EVENT_DATE");

        setContentView(R.layout.activity_students_roll);

        setTitle(getString(R.string.students));

        mRecyclerView = (RecyclerView)findViewById(R.id.StudentRecyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle args = new Bundle();
        args.putString("SCHOOL_ID", schoolId);
        args.putString("CLASS_ID", classId);
        args.putString("AUTH_ID", authToken);
        args.putString("ACC_TYPE", accType);

        Ion.with(this).load(getString(R.string.main_api_url) + "/class/makeCall?class_id=" + classId)
                .setHeader("Authentification", authToken)
                .setHeader("School", schoolId)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {

                if (e == null) {
                    System.out.println(result);

                    try {
                        JSONObject object = new JSONObject(result);
                        Iterator<?> keys = object.keys();

                        while (keys.hasNext()) {
                            String key = (String) keys.next();




                            if (object.get(key) instanceof JSONObject) {
                                Student student = new Student();
                                student.setId(key);
                                student.setBookId(((JSONObject) object.get(key)).get("book_id").toString());
                                student.setName(((JSONObject) object.get(key)).get("name").toString());
                                studentData.add(student);
                            }



                        }

                        if (mAdapter == null) {
                            // specify an adapter (see also next example)
                            mAdapter = new StudentRollAdapter(studentData);
                            mRecyclerView.setAdapter(mAdapter);
                        }
                    } catch (JSONException error) {
                        System.out.println("json error");
                    }

                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.student_roll_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {



        if (item.getTitle() == "Logout")
        {
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.clear();
            editor.commit();

            Intent intent   = new Intent(getApplication(), LoginActivity.class);

            startActivity(intent);
            finish();

        }
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            case R.id.action_cart:
                submitCall();
                return true;
        }


        return super.onOptionsItemSelected(item);
    }

    public void submitCall() {
        JsonObject json = new JsonObject();
        JsonArray list = new JsonArray();

        for (int i = 0; i < studentData.size(); i++) {
            JsonObject student = new JsonObject();

            student.addProperty("name", studentData.get(i).getName());
            student.addProperty("book_id", studentData.get(i).getBookId());
            student.addProperty("student_id", studentData.get(i).getId());
            student.addProperty("present", studentData.get(i).getPresent());
            list.add(student);
        }

        json.add("list", list);

        System.out.println("Absence list: " + json);
        System.out.println("Class id: " + classId);
        System.out.println("subject_id: " + subjectId);
        System.out.println("start: " + eventStart);
        System.out.println("end: " + eventEnd);
        System.out.println("date: " + eventDate);


        //Submit the list here
        Ion.with(this)
                .load(getString(R.string.main_api_url) + "/class/submitCall?class_id=" + classId)
                //.load(getString(R.string.main_api_url) + "/class/submitCall?class_id=" + classId)
                .setHeader("Authentification", authToken)
                .setHeader("School", schoolId)
                .setMultipartParameter("list", json.toString())
                .setMultipartParameter("subject_id", subjectId)
                .setMultipartParameter("start", eventStart)
                .setMultipartParameter("end", eventEnd)
                .setMultipartParameter("date", eventDate)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        if (e == null) {
                            System.out.println(result);
                            finish();
                        } else
                            System.out.println(e.getMessage());
                    }
                });
    }
}
