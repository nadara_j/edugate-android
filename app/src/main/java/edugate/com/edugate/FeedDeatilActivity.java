package edugate.com.edugate;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.Date;

import edugate.com.edugate.Object.Feed;
import edugate.com.edugate.Object.Mark;

public class FeedDeatilActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Feed feed = new Feed();
        feed.setTitle(getIntent().getStringExtra("FEED_TITLE"));
        feed.setContent(getIntent().getStringExtra("FEED_CONTENT"));
        feed.setDate(new Date(getIntent().getStringExtra("FEED_DATE")));
        feed.setId(getIntent().getStringExtra("FEED_ID"));
        feed.setClassId(getIntent().getStringExtra("FEED_CLASS"));

        final String schoolId = getIntent().getStringExtra("SCHOOL_ID");
        final String authId = getIntent().getStringExtra("AUTH_ID");


        setContentView(R.layout.activity_feed_detail);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        final EditText title = (EditText) findViewById(R.id.title);
        final EditText content = (EditText) findViewById(R.id.content);
        TextView date = (TextView) findViewById(R.id.date);

        Button edit = (Button) findViewById(R.id.editButton);
        Button delete = (Button) findViewById(R.id.deleteButton);

        title.setText(feed.getTitle());
        content.setText(feed.getContent());
        date.setText(feed.getDate().toString());

        final FeedDeatilActivity copy = this;

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ion.with(v.getContext()).load("PUT", getString(R.string.main_api_url) + "/feed?feed_id=" + feed.getId())
                        .setHeader("School", schoolId).setHeader("Authentification", authId).setHeader("Class", feed.getClassId())
                        .setBodyParameter("title", title.getText().toString()).setBodyParameter("content", content.getText().toString())
                        .asString().setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        System.out.println(result);
                        copy.finish();
                    }
                });
            }
        });



        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ion.with(v.getContext()).load("DELETE", getString(R.string.main_api_url) + "/feed?feed_id=" + feed.getId())
                        .setHeader("School", schoolId).setHeader("Authentification", authId).setHeader("Class", feed.getClassId())
                        .asString().setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        System.out.println(result);
                        copy.finish();
                    }
                });
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;


        }

        return super.onOptionsItemSelected(item);
    }
}
