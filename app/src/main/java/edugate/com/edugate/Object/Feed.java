package edugate.com.edugate.Object;

import java.util.Date;

/**
 * Created by Jonathan on 15/11/15.
 */
public class Feed {
    private String  title;
    private String  content;
    private Date    date;
    private String  id;
    private String  classId;

    public Feed() {
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
