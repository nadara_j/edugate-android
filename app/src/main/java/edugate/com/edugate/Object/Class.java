package edugate.com.edugate.Object;

/**
 * Created by Dark on 10/12/2015.
 */
public class Class {
    private String name;
    private String id;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
