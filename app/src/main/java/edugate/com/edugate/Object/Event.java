package edugate.com.edugate.Object;

/**
 * Created by Dark on 10/12/2015.
 */
public class Event {
    private String start;
    private String end;
    private String subjectId;
    private String classId;
    private String date;
    private String who;
    private String subject;
    private boolean calendar;

    public String getStart() {
        return this.start;
    }
    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return this.end;
    }
    public void setEnd(String end) {
        this.end = end;
    }

    public String getSubjectId() {
        return this.subjectId;
    }
    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getClassId() { return this.classId; }

    public void setClassId(String classId) { this.classId = classId; }

    public String getDate() { return this.date; }

    public void setDate(String date) { this.date = date; }

    public String getWho() {
        return who;
    }

    public void setWho(String who) {
        this.who = who;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public boolean isCalendar() {
        return calendar;
    }

    public void setCalendar(boolean calendar) {
        this.calendar = calendar;
    }
}
