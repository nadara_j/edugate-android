package edugate.com.edugate.Object;

import java.util.Date;

/**
 * Created by Dark on 19/11/2015.
 */
public class Message {
    private String  sender;
    private String  content;
    private Date date;
    private boolean isOwner;

    public Message() {
    }

    public String getSender() {
        return this.sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isOwner() {
        return isOwner;
    }

    public void setOwner(boolean owner) {
        isOwner = owner;
    }
}
