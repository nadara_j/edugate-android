package edugate.com.edugate.Object;

/**
 * Created by Dark on 10/12/2015.
 */
public class Student {
    private String  name;
    private String  bookId;
    private String  id;
    private boolean present;

    public Student() {
        this.present = false;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBookId() {
        return this.bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public boolean getPresent() { return this.present; }

    public void setPresent(boolean present) { this.present = present; }
}
