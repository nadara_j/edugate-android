package edugate.com.edugate;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import edugate.com.edugate.Object.Homework;

public class AddDetentionActivity extends AppCompatActivity {
    private ArrayList<String>   subjectArray    = new ArrayList<>();
    private ArrayList<Homework> homeworkArray   = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_detention);

        final Spinner subjectList               = (Spinner)findViewById(R.id.subjectList);

        final String school                     = getIntent().getStringExtra("SCHOOL_ID");
        final String bookId                     = getIntent().getStringExtra("STUDENT_ID");
        final String subject_id                 = getIntent().getStringExtra("SUBJECT_ID");
        final String authId                     = getIntent().getStringExtra("AUTH_ID");

        final Activity backup                   = this;


        setButtonActionWithHomework();


    }

    public void setButtonActionWithHomework() {
        final String school                     = getIntent().getStringExtra("SCHOOL_ID");
        final String bookId                     = getIntent().getStringExtra("STUDENT_ID");
        final String subject_id                 = getIntent().getStringExtra("SUBJECT_ID");
        final String authId                     = getIntent().getStringExtra("AUTH_ID");



        final EditText detentionComment         = (EditText)findViewById(R.id.detentionComment);
        Button   submitButton                   = (Button)findViewById(R.id.submitDetentionButton);
        final DatePicker datePicker             = (DatePicker)findViewById(R.id.datePicker1);
        final TimePicker timePicker             = (TimePicker)findViewById(R.id.timePicker1);


        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                if (detentionComment.getText().toString().equals("")) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(view.getContext());
                    dialog.setTitle(getString(R.string.error_field_required));
                    dialog.setMessage(getString(R.string.error_field_required));
                    dialog.show();
                }
                else {

                    int day = datePicker.getDayOfMonth();
                    int month = datePicker.getMonth() + 1;
                    int year =  datePicker.getYear();

                    int hour = timePicker.getCurrentHour();
                    int min = timePicker.getCurrentMinute();

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
                    String currentDateandTime = sdf.format(new Date());
                    System.out.println(currentDateandTime);


                    String sb = String.valueOf(year) + "-" + String.valueOf(month) + "-" +  String.valueOf(day) + "T" + String.valueOf(hour) + ":" + String.valueOf(min) + ":" + "00:000Z";
                    String sb2 = String.valueOf(year) + "-" + String.valueOf(month) + "-" +  String.valueOf(day) + "T" + String.valueOf(hour + 1) + ":" + String.valueOf(min) + ":" + "00:000Z";

                    System.out.println("timestamp: " + sb);
                    Ion.with(view.getContext())
                            .load("PUT", getString(R.string.main_api_url) + "/book/addDetention?book_id=" + bookId)
                            .setHeader("School", school)
                            .setHeader("Authentification", authId)
                            .setBodyParameter("subject_id", subject_id)
                            .setBodyParameter("teacher_id", authId)
                            .setBodyParameter("content", detentionComment.getText().toString())
                            .setBodyParameter("start", sb)
                            .setBodyParameter("end", sb2)
                            .asJsonObject()
                            .setCallback(new FutureCallback<JsonObject>() {
                                @Override
                                public void onCompleted(Exception e, JsonObject result) {
                                    if (e == null) {
                                        System.out.println(result);
                                        if (result.has("message")) {
                                            finish();
                                        } else
                                            Toast.makeText(view.getContext(), "Error", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                }
            }
        });
    }
}
