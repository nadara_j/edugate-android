package edugate.com.edugate;

import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import edugate.com.edugate.Object.Mark;

public class MarkDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Mark mark = new Mark();
        mark.setName(getIntent().getStringExtra("MARK_NAME"));
        mark.setMark(getIntent().getStringExtra("MARK_VALUE"));
        mark.setId(getIntent().getStringExtra("MARK_ID"));
        mark.setCoefficient(getIntent().getStringExtra("MARK_COEFF"));
        mark.setComment(getIntent().getStringExtra("MARK_COMMENT"));
        mark.setStudentId(getIntent().getStringExtra("STUDENT_ID"));
        final String schoolId = getIntent().getStringExtra("SCHOOL_ID");
        final String authId = getIntent().getStringExtra("AUTH_ID");


        setContentView(R.layout.activity_mark_detail);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        final EditText markValue = (EditText) findViewById(R.id.markValue);
        TextView markName = (TextView) findViewById(R.id.markName);

        Button edit = (Button) findViewById(R.id.editButton);
        Button delete = (Button) findViewById(R.id.deleteButton);

        markValue.setText(mark.getMark());
        markName.setText(mark.getName());

        final MarkDetailActivity copy = this;

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ion.with(v.getContext()).load("PUT", getString(R.string.main_api_url) + "/book/updateNote?book_id=" + mark.getStudentId() + "&note_id=" + mark.getId())
                        .setHeader("School", schoolId).setHeader("Authentification", authId)
                        .setBodyParameter("title", mark.getName()).setBodyParameter("comment", mark.getComment())
                        .setBodyParameter("value", markValue.getText().toString()).setBodyParameter("coefficient", mark.getCoefficient())
                        .asString().setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        System.out.println(result);
                        copy.finish();
                    }
                });
            }
        });



        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ion.with(v.getContext()).load("DELETE", getString(R.string.main_api_url) + "/book/deleteNote?book_id=" + mark.getStudentId() + "&note_id=" + mark.getId())
                        .setHeader("School", schoolId).setHeader("Authentification", authId)
                        .asString().setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        System.out.println(result);
                        copy.finish();
                    }
                });
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;


        }

        return super.onOptionsItemSelected(item);
    }
}
