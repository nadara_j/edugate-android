package edugate.com.edugate.Fragment;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import edugate.com.edugate.Adapter.EventAdapter;
import edugate.com.edugate.Object.Event;
import edugate.com.edugate.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CalendarFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CalendarFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CalendarFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private ArrayList<Event> eventData = new ArrayList<>();

    private OnFragmentInteractionListener mListener;

    public CalendarFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CalendarFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CalendarFragment newInstance(String param1, String param2) {
        CalendarFragment fragment = new CalendarFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_calendar, container, false);


        final String planningId   = getArguments().getString("PLANNING_ID");
        final String authId       = getArguments().getString("AUTH_ID");
        final String schoolId     = getArguments().getString("SCHOOL_ID");
        final String accType     = getArguments().getString("ACC_TYPE");

        mRecyclerView = (RecyclerView)view.findViewById(R.id.eventList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);


        System.out.println(getString(R.string.main_api_url) + "/planning?planning_id=" + planningId);




        CalendarView calendarView = (CalendarView) view.findViewById(R.id.calendarView);

        System.out.println(calendarView.getDate());
        eventData.clear();

        Calendar c = Calendar.getInstance();
        c.setTime(new Date(calendarView.getDate()));

        final int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

        System.out.println(dayOfWeek);
        Ion.with(getContext())
                .load(getString(R.string.main_api_url) + "/planning/own")
                .setHeader("Authentification", authId)
                .setHeader("School", schoolId)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        if (e == null) {
                            System.out.println(result);
                            eventData.clear();
                            try {
                                JSONObject object = new JSONObject(result);
                                Iterator<?> keys = object.keys();
                                Gson gson = new Gson();

                                while (keys.hasNext()) {
                                    String key = (String) keys.next();
                                    if (object.get(key) instanceof JSONObject && key.equals("event_list")) {
                                        System.out.println(key);

                                        object = (JSONObject) (object.get(key));
                                        keys = object.keys();
                                        gson = new Gson();

                                        while (keys.hasNext()) {
                                            key = (String) keys.next();
                                            if (object.get(key) instanceof JSONArray &&
                                                    ((key.equals("mon") && dayOfWeek == 6)
                                                            || (key.equals("tue") && dayOfWeek == 7)
                                                            || (key.equals("wed") && dayOfWeek == 1)
                                                            || (key.equals("thu") && dayOfWeek == 2)
                                                            || (key.equals("fri") && dayOfWeek == 3)
                                                            || (key.equals("sat") && dayOfWeek == 4)
                                                            || (key.equals("sun") && dayOfWeek == 5)
                                                    ))
                                            {
                                                JSONArray events = (JSONArray) object.get(key);

                                                for (int i = 0; i < events.length(); i++) {
                                                    Event event = new Event();
                                                    event.setSubjectId(events.getJSONObject(i).get("subject_id").toString());
                                                    event.setStart(events.getJSONObject(i).get("start").toString());
                                                    event.setCalendar(true);
                                                    event.setEnd(events.getJSONObject(i).get("end").toString());
                                                    event.setSubject(events.getJSONObject(i).get("subject_name").toString());
                                                    if (accType.equals("Teacher"))
                                                        event.setWho(events.getJSONObject(i).get("class_name").toString());
                                                    else
                                                        event.setWho(events.getJSONObject(i).get("teacher_name").toString());
                                                    event.setClassId(events.getJSONObject(i).get("class_id").toString());
                                                    //event.setDate(i2 + "/" + (i1 + 1) + "/" + i);
                                                    eventData.add(event);
                                                }
                                            }

                                        }
                                    }
                                }
                            } catch (JSONException ex) {
                                System.out.println("execption");
                            }
                            System.out.println(eventData.size());
                            if (mAdapter == null) {
                                // specify an adapter (see also next example)
                                mAdapter = new EventAdapter(eventData);
                                mRecyclerView.setAdapter(mAdapter);
                            } else
                                mAdapter.notifyDataSetChanged();
                        }
                    }
                });

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView calendarView, final int i, final int i1, final int i2) {



                Calendar c = Calendar.getInstance();
                c.setTime(new Date(i, i1 + 1, i2));

                final int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

                Ion.with(getContext())
                        .load(getString(R.string.main_api_url) + "/planning/own")
                        .setHeader("Authentification", authId)
                        .setHeader("School", schoolId)
                        .asString()
                        .setCallback(new FutureCallback<String>() {
                            @Override
                            public void onCompleted(Exception e, String result) {
                                if (e == null) {
                                    System.out.println(result);
                                    eventData.clear();
                                    try {
                                        JSONObject object = new JSONObject(result);
                                        Iterator<?> keys = object.keys();
                                        Gson gson = new Gson();

                                        while (keys.hasNext()) {
                                            String key = (String) keys.next();
                                            if (object.get(key) instanceof JSONObject && key.equals("event_list")) {
                                                System.out.println(key);

                                                object = (JSONObject) (object.get(key));
                                                keys = object.keys();
                                                gson = new Gson();

                                                while (keys.hasNext()) {
                                                    key = (String) keys.next();
                                                    if (object.get(key) instanceof JSONArray &&
                                                            ((key.equals("mon") && dayOfWeek == 6)
                                                                    || (key.equals("tue") && dayOfWeek == 7)
                                                                    || (key.equals("wed") && dayOfWeek == 1)
                                                                    || (key.equals("thu") && dayOfWeek == 2)
                                                                    || (key.equals("fri") && dayOfWeek == 3)
                                                                    || (key.equals("sat") && dayOfWeek == 4)
                                                                    || (key.equals("sun") && dayOfWeek == 5)
                                                            ))
                                                    {
                                                        JSONArray events = (JSONArray) object.get(key);

                                                        for (int i = 0; i < events.length(); i++) {
                                                            Event event = new Event();
                                                            event.setSubjectId(events.getJSONObject(i).get("subject_id").toString());
                                                            event.setStart(events.getJSONObject(i).get("start").toString());
                                                            event.setCalendar(true);
                                                            event.setEnd(events.getJSONObject(i).get("end").toString());
                                                            event.setSubject(events.getJSONObject(i).get("subject_name").toString());
                                                            if (accType.equals("Teacher"))
                                                                event.setWho(events.getJSONObject(i).get("class_name").toString());
                                                            else
                                                                event.setWho(events.getJSONObject(i).get("teacher_name").toString());
                                                            event.setClassId(events.getJSONObject(i).get("class_id").toString());
                                                            event.setDate(i2 + "/" + (i1 + 1) + "/" + i);
                                                            eventData.add(event);
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                    } catch (JSONException ex) {
                                        System.out.println("execption");
                                    }

                                    if (mAdapter == null) {
                                        // specify an adapter (see also next example)
                                        mAdapter = new EventAdapter(eventData);
                                        mRecyclerView.setAdapter(mAdapter);
                                    } else
                                        mAdapter.notifyDataSetChanged();
                                }
                            }
                        });
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
