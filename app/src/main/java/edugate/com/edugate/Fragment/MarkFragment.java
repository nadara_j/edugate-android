package edugate.com.edugate.Fragment;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import edugate.com.edugate.Adapter.ClassAdapter;
import edugate.com.edugate.Adapter.MarkAdapter;
import edugate.com.edugate.Adapter.SubjectAdapter;
import edugate.com.edugate.Object.Class;
import edugate.com.edugate.Object.Mark;
import edugate.com.edugate.Object.Subject;
import edugate.com.edugate.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MarkFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MarkFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MarkFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private ArrayList<Class> classData = new ArrayList<>();

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public MarkFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MarkFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MarkFragment newInstance(String param1, String param2) {
        MarkFragment fragment = new MarkFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view;
        String acc_type = getArguments().getString("ACC_TYPE");
        final String auth_id = getArguments().getString("AUTH_ID");
        String school_id = getArguments().getString("SCHOOL_ID");
        System.out.println(acc_type);

        getActivity().getIntent().putExtra("TRANSACTION_TYPE", "MARK");

        if (acc_type.equals("Teacher"))
        {
            getActivity().setTitle(getString(R.string.subjects));
            view = inflater.inflate(R.layout.fragment_mark_teacher, container, false);

            mRecyclerView = (RecyclerView)view.findViewById(R.id.SubjectRecyclerView);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            mRecyclerView.setLayoutManager(mLayoutManager);

            Ion.with(this).load(getString(R.string.main_api_url) + "/subjects")
                    .setHeader("Authentification", auth_id)
                    .setHeader("School", school_id)
                    .asString().setCallback(new FutureCallback<String>() {
                @Override
                public void onCompleted(Exception e, String result) {
                    if (e == null)
                    {
                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" + result);
                        try {
                            JSONObject object = new JSONObject(result);
                            Iterator<?> keys = object.keys();
                            Gson gson = new Gson();

                            ArrayList<String> classArrayCheck = new ArrayList<String>();

                            while (keys.hasNext()) {
                                String key = (String) keys.next();
                                System.out.print(object.get(key));

                                JSONObject subject = ((JSONObject) object.get(key));
                                Iterator<?> keysSubject = subject.keys();

                                System.out.println(subject);
                                while (keysSubject.hasNext())
                                {

                                    String keySubject = (String) keysSubject.next();
                                    System.out.println(keySubject);
                                    if (keySubject.equals("teacher_id")) {
                                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" + subject.get(keySubject).toString() + " == " + auth_id);
                                        if (subject.get(keySubject).toString().equals(auth_id)) {
                                            System.out.println((subject).get("class_list"));

                                            JSONObject classs = (JSONObject)(subject).get("class_list");
                                            Iterator<?> classKey = classs.keys();

                                            while (classKey.hasNext()) {
                                                String keyClass = (String) classKey.next();
                                                if (classs.get(keyClass) instanceof JSONObject) {

                                                    if (!classArrayCheck.contains(keyClass)) {
                                                        classArrayCheck.add(keyClass);
                                                        Class classe = new Class();
                                                        classe.setId(keyClass);
                                                        System.out.println("CLASSID = " + keyClass);
                                                        classe.setName(((JSONObject) classs.get(keyClass)).get("name").toString());
                                                        classData.add(classe);
                                                    }

                                                }
                                            }
                                        }


                                    }
                                }

                            }

                            mAdapter = new ClassAdapter(classData);
                            mRecyclerView.setAdapter(mAdapter);

                        }
                        catch (JSONException error) {
                            System.out.println("json error");
                        }
                    }
                }
            });
        }
        else if (acc_type.equals("Student") || acc_type.equals("Parent")) {

            view = inflater.inflate(R.layout.fragment_mark_student, container, false);
            final View viewCopy = view;

            mRecyclerView = (RecyclerView)view.findViewById(R.id.MarkRecyclerView);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            mRecyclerView.setLayoutManager(mLayoutManager);


            Ion.with(getContext())
                    .load(getString(R.string.main_api_url) + "/student?student_id=" + getArguments().getString("AUTH_ID"))
                    .setHeader("Authentification", getArguments().getString("AUTH_ID"))
                    .setHeader("School", getArguments().getString("SCHOOL_ID"))
                    .setHeader("Class", getArguments().getString("CLASS_ID"))
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            if (e == null) {
                                System.out.println(result);

                                String studentBookID;

                                if (result.has("book_id")) {
                                    studentBookID = result.get("book_id").getAsString();
                                    getStudentBookDetails(viewCopy, studentBookID);
                                }
                            }

                        }
                    });
        }
        else {
            view = inflater.inflate(R.layout.fragment_mark, container, false);
        }
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void getStudentBookDetails(View view, String studentBookID) {

        String auth_id = getArguments().getString("AUTH_ID");
        String school_id = getArguments().getString("SCHOOL_ID");
        final String acc_type = getArguments().getString("ACC_TYPE");



        final ArrayList <Mark> markData = new ArrayList<>();

        System.out.println(getString(R.string.main_api_url) + "/book?book_id=" + studentBookID);

        Ion.with(this).load(getString(R.string.main_api_url) + "/book?book_id=" + studentBookID)
                .setHeader("Authentification", auth_id)
                .setHeader("School", school_id)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                if (e == null) {

                    try {
                        JSONObject object = new JSONObject(result);
                        Iterator<?> keys = object.keys();
                        Gson gson = new Gson();


                        while (keys.hasNext()) {
                            String key = (String) keys.next();
                            if (object.get(key) instanceof JSONObject && key.equals("notes")) {

                                object = (JSONObject) (object.get(key));
                                keys = object.keys();

                                while (keys.hasNext()) {
                                    key = (String) keys.next();

                                    if (object.get(key) != null) {

                                        if (object.get(key) instanceof JSONObject) {

                                            Mark mark = new Mark();
                                            mark.setId(key);
                                            mark.setName(((JSONObject) object.get(key)).get("title").toString());
                                            mark.setMark(((JSONObject) object.get(key)).get("value").toString());
                                            System.out.println(mark.getName());
                                            markData.add(mark);
                                        }
                                    }
                                }

                            }
                        }

                        MarkAdapter adapter = new MarkAdapter(markData);
                        adapter.setAccType(acc_type);
                        mAdapter = adapter;
                        mRecyclerView.setAdapter(mAdapter);

                        System.out.println("count " + markData.size());

                    } catch (JSONException error) {
                        System.out.println("json error");
                    }
                }
            }
        });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
