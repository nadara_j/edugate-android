package edugate.com.edugate.Fragment;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import edugate.com.edugate.Adapter.ReportAdapter;
import edugate.com.edugate.Object.Report;
import edugate.com.edugate.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ReportFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ReportFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReportFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private ArrayList<Report> reportData = new ArrayList<>();

    public ReportFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ReportFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ReportFragment newInstance(String param1, String param2) {
        ReportFragment fragment = new ReportFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }


    public void getBookDetails(String studentBookID) {

        Ion.with(getContext())
                .load(getString(R.string.main_api_url) + "/book?book_id=" + studentBookID)
                .setHeader("Authentification", getArguments().getString("AUTH_ID"))
                .setHeader("School", getArguments().getString("SCHOOL_ID"))
                .setHeader("Class", getArguments().getString("CLASS_ID"))
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        if (e == null) {
                            System.out.println(result);


                            /*
                            if (result.has("notes")) {
                                JsonArray notes = result.getAsJsonArray("notes");

                                for (int i = 0; i < notes.size(); i++) {
                                    if (notes.get(i).isJsonNull())
                                        continue;

                                    Report report = new Report();

                                    report.setTitle("Note");

                                    if (notes.get(i).getAsJsonObject().has("comment"))
                                        report.setContent(notes.get(i).getAsJsonObject().get("comment").getAsString());
                                    else
                                        report.setContent("No comment");
                                    report.setDate(new Date());

                                    reportData.add(report);
                                }
                            }


                            if (mAdapter == null) {
                                // specify an adapter (see also next example)
                                mAdapter = new ReportAdapter(reportData);
                                mRecyclerView.setAdapter(mAdapter);
                            }*/

                            try {
                                JSONObject object = new JSONObject(result);
                                Iterator<?> keys = object.keys();
                                Gson gson = new Gson();

                                boolean detentions = false;
                                boolean absences = false;
                                boolean comments = false;



                                while (keys.hasNext() && (detentions == false || absences == false || comments == false)) {
                                    String key = (String) keys.next();

                                    if (object.get(key) instanceof JSONObject && key.equals("absences") && absences == false) {
                                        absences = true;
                                        object = (JSONObject) (object.get(key));
                                        keys = object.keys();

                                        while (keys.hasNext()) {
                                            key = (String) keys.next();

                                            if (object.get(key) != null) {

                                                if (object.get(key) instanceof JSONObject) {

                                                    Report report = new Report();

                                                    report.setTitle("Absence ou Retard");
                                                    report.setContent(((JSONObject) object.get(key)).get("comment").toString());
                                                    report.setDate(new Date());
                                                    reportData.add(report);
                                                }
                                            }
                                        }
                                        object = new JSONObject(result);
                                        keys = object.keys();
                                        key = (String) keys.next();


                                    }
                                    if (object.get(key) instanceof JSONObject && key.equals("comments") && comments == false) {
                                        comments = true;
                                        object = (JSONObject) (object.get(key));
                                        keys = object.keys();

                                        while (keys.hasNext()) {
                                            key = (String) keys.next();

                                            if (object.get(key) != null) {

                                                if (object.get(key) instanceof JSONObject) {

                                                    Report report = new Report();

                                                    report.setTitle("Mot");
                                                    report.setContent(((JSONObject) object.get(key)).get("content").toString());
                                                    report.setDate(new Date());
                                                    reportData.add(report);
                                                }
                                            }
                                        }
                                        object = new JSONObject(result);
                                        keys = object.keys();
                                        key = (String) keys.next();
                                    }
                                    if (object.get(key) instanceof JSONObject && key.equals("detentions") && detentions == false) {
                                        detentions = true;
                                        object = (JSONObject) (object.get(key));
                                        keys = object.keys();


                                        while (keys.hasNext()) {
                                            key = (String) keys.next();

                                            if (object.get(key) != null) {

                                                if (object.get(key) instanceof JSONObject) {

                                                    Report report = new Report();

                                                    report.setTitle("Heure de colle");
                                                    report.setContent(((JSONObject) object.get(key)).get("start").toString() + "\n" + ((JSONObject) object.get(key)).get("end").toString());
                                                    report.setDate(new Date());
                                                    reportData.add(report);
                                                }
                                            }
                                        }
                                        object = new JSONObject(result);
                                        keys = object.keys();
                                        key = (String) keys.next();

                                    }
                                }

                                // specify an adapter (see also next example)
                                mAdapter = new ReportAdapter(reportData);
                                mRecyclerView.setAdapter(mAdapter);
                            } catch (JSONException error) {
                                System.out.println("json error");
                            }

                        }
                    }
                });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View reportView = inflater.inflate(R.layout.fragment_report, container, false);

        mRecyclerView = (RecyclerView)reportView.findViewById(R.id.homeReportRecyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);



        Ion.with(getContext())
                .load(getString(R.string.main_api_url) + "/student?student_id=" +  getArguments().getString("AUTH_ID"))
                .setHeader("Authentification", getArguments().getString("AUTH_ID"))
                .setHeader("School", getArguments().getString("SCHOOL_ID"))
                .setHeader("Class", getArguments().getString("CLASS_ID"))
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (e == null) {
                            System.out.println(result);

                            String studentBookID = "";

                            if (result.has("book_id"))
                                studentBookID = result.get("book_id").getAsString();

                            getBookDetails(studentBookID);
                        }

                    }



                });

        // Change ID when available ???????????????????????????????????????????????????????


        return reportView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
