package edugate.com.edugate.Fragment;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import edugate.com.edugate.Adapter.HomeworkAdapter;
import edugate.com.edugate.Adapter.MarkAdapter;
import edugate.com.edugate.Adapter.SubjectAdapter;
import edugate.com.edugate.Object.Homework;
import edugate.com.edugate.Object.Mark;
import edugate.com.edugate.Object.Subject;
import edugate.com.edugate.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeworkFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeworkFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeworkFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private ArrayList<Subject> subjectData = new ArrayList<>();

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public HomeworkFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MarkFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeworkFragment newInstance(String param1, String param2) {
        HomeworkFragment fragment = new HomeworkFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view;
        String acc_type = getArguments().getString("ACC_TYPE");
        String auth_id = getArguments().getString("AUTH_ID");
        String school_id = getArguments().getString("SCHOOL_ID");
        System.out.println(acc_type);

        if (acc_type.equals("Teacher"))
        {
            //Bundle args = new Bundle();
            //args.putString("TRANSACTION_TYPE", "DETENTION");

            getActivity().getIntent().putExtra("TRANSACTION_TYPE", "HOMEWORK");
            //getArguments().putString("TRANSACTION_TYPE", "DETENTION");

            view = inflater.inflate(R.layout.fragment_mark_teacher, container, false);

            mRecyclerView = (RecyclerView)view.findViewById(R.id.SubjectRecyclerView);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            mRecyclerView.setLayoutManager(mLayoutManager);

            Ion.with(this).load(getString(R.string.main_api_url) + "/teacher?teacher_id=" + auth_id)
                    .setHeader("Authentification", auth_id)
                    .setHeader("School", school_id)
                    .asString().setCallback(new FutureCallback<String>() {
                @Override
                public void onCompleted(Exception e, String result) {
                    if (e == null)
                    {

                        try {
                            JSONObject object = new JSONObject(result);
                            Iterator<?> keys = object.keys();
                            Gson gson = new Gson();


                            while (keys.hasNext()) {
                                String key = (String) keys.next();
                                if (object.get(key) instanceof JSONObject && key.equals("subject_list")) {
                                    System.out.println(key);

                                    object = (JSONObject)(object.get(key));
                                    keys = object.keys();
                                    gson = new Gson();
                                    while (keys.hasNext()) {
                                        key = (String) keys.next();
                                        if (object.get(key) instanceof JSONObject) {
                                            Subject subject = new Subject();
                                            subject.setId(key);
                                            subject.setName(((JSONObject) object.get(key)).get("subject_name").toString());
                                            subjectData.add(subject);
                                        }
                                    }

                                }
                            }

                            mAdapter = new SubjectAdapter(subjectData);
                            mRecyclerView.setAdapter(mAdapter);

                        }
                        catch (JSONException error) {
                            System.out.println("json error");
                        }
                    }
                }
            });
        }
        else if (acc_type.equals("Student") || acc_type.equals("Parent"))
        {
            view = inflater.inflate(R.layout.fragment_homework, container, false);

            mRecyclerView = (RecyclerView)view.findViewById(R.id.HomeworkRecyclerView);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            mRecyclerView.setLayoutManager(mLayoutManager);

            final ArrayList<Homework> homeworks = new ArrayList<>();

            Ion.with(this).load(getString(R.string.main_api_url) + "/student/getHomeworks")
                    .setHeader("Authentification", auth_id)
                    .setHeader("School", school_id)
                    .asString().setCallback(new FutureCallback<String>() {
                @Override
                public void onCompleted(Exception e, String result) {
                    if (e == null)
                    {

                        try {
                            System.out.println(result);

                            JSONArray object = new JSONArray(result);
                            //Iterator<?> keys = object.keys();
                            //Gson gson = new Gson();

                            System.out.println(object);

                            for (int i = 0; i < object.length(); i++) {
                                System.out.println(object.getJSONObject(i));

                                Homework homework = new Homework();
                                homework.setName(object.getJSONObject(i).getString("title"));
                                homework.setDesc(object.getJSONObject(i).getString("description"));
                                homework.setEndDate(object.getJSONObject(i).getString("end"));

                                homeworks.add(homework);


                            }

                            /*
                            while (keys.hasNext()) {
                                String key = (String) keys.next();
                                if (object.get(key) instanceof JSONObject) {


                                    System.out.println((JSONObject) object.get(key));



                                }
                            }
                            */

                            mAdapter = new HomeworkAdapter(homeworks);
                            mRecyclerView.setAdapter(mAdapter);

                        }
                        catch (JSONException error) {
                            System.out.println("json error");
                        }
                    }
                }
            });
        }
        else {
            view = inflater.inflate(R.layout.fragment_mark, container, false);
        }
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void getStudentBookDetails(View view, String studentBookID) {

        String auth_id = getArguments().getString("AUTH_ID");
        String school_id = getArguments().getString("SCHOOL_ID");




        final ArrayList <Mark> markData = new ArrayList<>();

        System.out.println(getString(R.string.main_api_url) + "/book?book_id=" + studentBookID);

        Ion.with(this).load(getString(R.string.main_api_url) + "/book?book_id=" + studentBookID)
                .setHeader("Authentification", auth_id)
                .setHeader("School", school_id)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                if (e == null) {

                    try {
                        JSONObject object = new JSONObject(result);
                        Iterator<?> keys = object.keys();
                        Gson gson = new Gson();


                        while (keys.hasNext()) {
                            String key = (String) keys.next();
                            if (object.get(key) instanceof JSONObject && key.equals("notes")) {

                                object = (JSONObject) (object.get(key));
                                keys = object.keys();

                                while (keys.hasNext()) {
                                    key = (String) keys.next();

                                    if (object.get(key) != null) {

                                        if (object.get(key) instanceof JSONObject) {

                                            Mark mark = new Mark();
                                            mark.setId(key);
                                            mark.setName(((JSONObject) object.get(key)).get("title").toString());
                                            mark.setMark(((JSONObject) object.get(key)).get("value").toString());
                                            System.out.println(mark.getName());
                                            markData.add(mark);
                                        }
                                    }
                                }

                            }
                        }

                        mAdapter = new MarkAdapter(markData);
                        mRecyclerView.setAdapter(mAdapter);

                        System.out.println("count " + markData.size());

                    } catch (JSONException error) {
                        System.out.println("json error");
                    }
                }
            }
        });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
