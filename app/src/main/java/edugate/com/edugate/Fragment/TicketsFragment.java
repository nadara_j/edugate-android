package edugate.com.edugate.Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import edugate.com.edugate.Adapter.TicketAdapter;
import edugate.com.edugate.AddTicketsActivity;
import edugate.com.edugate.Object.Ticket;
import edugate.com.edugate.R;
import edugate.com.edugate.SubjectActivity;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TicketsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TicketsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TicketsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private String schoolId;
    private String authToken;
    private String accType;

    private OnFragmentInteractionListener mListener;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private ArrayList<Ticket> ticketData = new ArrayList<>();

    public TicketsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ReportFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TicketsFragment newInstance(String param1, String param2) {
        TicketsFragment fragment = new TicketsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View reportView = inflater.inflate(R.layout.fragment_tickets, container, false);

        mRecyclerView = (RecyclerView)reportView.findViewById(R.id.TicketsRecyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        getActivity().setTitle(getString(R.string.tickets));

        accType = getArguments().getString("ACC_TYPE");
        authToken = getArguments().getString("AUTH_ID");
        schoolId = getArguments().getString("SCHOOL_ID");


        Bundle args = new Bundle();
        args.putString("SCHOOL_ID", schoolId);
        args.putString("AUTH_ID", authToken);
        args.putString("ACC_TYPE", accType);

        FloatingActionButton addButton = (FloatingActionButton) reportView.findViewById(R.id.fab);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (accType.equals("Teacher")) {
                    Intent intent = new Intent(view.getContext(), SubjectActivity.class);
                    intent.putExtra("SCHOOL_ID", schoolId);
                    intent.putExtra("AUTH_ID", authToken);
                    intent.putExtra("ACC_TYPE", accType);
                    intent.putExtra("TRANSACTION_TYPE", "TICKET");

                    System.out.println(authToken);
                    System.out.println(schoolId);

                    view.getContext().startActivity(intent);
                }
                else if (accType.equals("Student")  || accType.equals("Parent")) {
                    final AlertDialog.Builder dialog = new AlertDialog.Builder(view.getContext());

                    Ion.with(view.getContext())
                            .load(getString(R.string.main_api_url) + "/student/getTeachers?student_id=" + authToken)
                            .setHeader("Authentification", getArguments().getString("AUTH_ID"))
                            .setHeader("School", getArguments().getString("SCHOOL_ID"))
                            .asString()
                            .setCallback(new FutureCallback<String>() {
                                @Override
                                public void onCompleted(Exception e, String result) {
                                    if (e == null) {

                                        List<String> items = new ArrayList<String>();
                                        final List<String> subject_id_array = new ArrayList<String>();
                                        final List<String> teacher_id_array = new ArrayList<String>();



                                        try {
                                            JSONObject object = new JSONObject(result);
                                            Iterator<?> keys = object.keys();
                                            Gson gson = new Gson();

                                            String key;

                                            while (keys.hasNext()) {
                                                key = (String) keys.next();

                                                if (object.get(key) != null) {
                                                    if (object.get(key) instanceof JSONObject) {
                                                        items.add(((JSONObject) object.get(key)).get("teacher_name").toString());
                                                        subject_id_array.add(((JSONObject) object.get(key)).get("subject_id").toString());
                                                        teacher_id_array.add(key);
                                                    }
                                                }
                                            }
                                        }
                                        catch (JSONException ex) {
                                            Log.d("error", "error json");
                                        }
                                        dialog.setSingleChoiceItems(items.toArray(new String[items.size()]), 0, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                                Intent intent = new Intent(getContext(), AddTicketsActivity.class);
                                                intent.putExtra("AUTH_ID", authToken);
                                                intent.putExtra("SCHOOL_ID", schoolId);
                                                intent.putExtra("ACC_TYPE", accType);
                                                intent.putExtra("STUDENT_ID", teacher_id_array.get(i));
                                                intent.putExtra("SUBJECT_ID", subject_id_array.get(i));
                                                intent.putExtra("TRANSACTION_TYPE", "TICKET");

                                                getContext().startActivity(intent);
                                            }
                                        });

                                        dialog.show();
                                    }
                                }
                            });
                }
            }
        });

        Ion.with(getContext())
                .load(getString(R.string.main_api_url) + "/tickets")
                .setHeader("Authentification", getArguments().getString("AUTH_ID"))
                .setHeader("School", getArguments().getString("SCHOOL_ID"))
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        if (e == null) {
                            System.out.println(result);

                            try {
                                JSONObject object = new JSONObject(result);
                                Iterator<?> keys = object.keys();
                                Gson gson = new Gson();

                                String key;

                                while (keys.hasNext()) {
                                    key = (String) keys.next();

                                    if (object.get(key) != null) {

                                        if (object.get(key) instanceof JSONObject) {

                                            Ticket ticket = new Ticket();
                                            ticket.setId(key);
                                            ticket.setContent(((JSONObject) object.get(key)).get("content").toString());
                                            ticket.setTitle(((JSONObject) object.get(key)).get("subject").toString());
                                            ticket.setDate(((JSONObject) object.get(key)).get("last_update").toString());
                                            ticket.setReceiverName(((JSONObject) object.get(key)).get("receiver_name").toString());
                                            ticket.setSenderName(((JSONObject) object.get(key)).get("sender_name").toString());
                                            ticket.setSenderId(((JSONObject) object.get(key)).get("sender_id").toString());
                                            ticket.setOpen((boolean) ((JSONObject) object.get(key)).get("open"));
                                            ticketData.add(ticket);
                                        }
                                    }

                                }

                                mAdapter = new TicketAdapter(ticketData, authToken);
                                mRecyclerView.setAdapter(mAdapter);
                            } catch (JSONException error) {
                                System.out.println("json error");
                            }
                        }

                    }


                });

        // Change ID when available ???????????????????????????????????????????????????????


        return reportView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
