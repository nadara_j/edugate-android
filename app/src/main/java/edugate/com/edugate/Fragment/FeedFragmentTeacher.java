package edugate.com.edugate.Fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import edugate.com.edugate.Adapter.FeedAdapter;
import edugate.com.edugate.Object.Feed;
import edugate.com.edugate.R;
import edugate.com.edugate.SubjectActivity;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FeedFragmentTeacher.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FeedFragmentTeacher#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FeedFragmentTeacher extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RecyclerView mRecyclerView;
    private FeedAdapter mAdapter;
    private ArrayList<Feed> feedData = new ArrayList<>();

    private View view;


    private OnFragmentInteractionListener mListener;

    public FeedFragmentTeacher() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FeedFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FeedFragmentTeacher newInstance(String param1, String param2) {
        FeedFragmentTeacher fragment = new FeedFragmentTeacher();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        getFeeds(this.view);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        this.view = inflater.inflate(R.layout.fragment_feed_teacher, container, false);
        FloatingActionButton addButton = (FloatingActionButton) this.view.findViewById(R.id.fab);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(view.getContext(), SubjectActivity.class);
                intent.putExtra("SCHOOL_ID", getArguments().getString("SCHOOL_ID"));
                intent.putExtra("AUTH_ID", getArguments().getString("AUTH_ID"));
                intent.putExtra("ACC_TYPE", getArguments().getString("ACC_TYPE"));
                intent.putExtra("TRANSACTION_TYPE", "FEED");


                view.getContext().startActivity(intent);
            }
        });
        return this.view;
    }

    public void getFeeds(View view)
    {


        feedData.clear();

        System.out.println("getfeeds");

        mRecyclerView = (RecyclerView)view.findViewById(R.id.homeFeedRecyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        Ion.with(getContext())
                .load(getString(R.string.main_api_url) + "/feeds/getGivenFeeds")
                .setHeader("Authentification", getArguments().getString("AUTH_ID"))
                .setHeader("School", getArguments().getString("SCHOOL_ID"))
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        if (e == null) {
                            System.out.println("feed result : " + result);
                            try {
                                JSONObject object = new JSONObject(result);

                                Iterator<?> keys = object.keys();
                                Gson gson = new Gson();


                                while (keys.hasNext()) {


                                    String key = (String) keys.next();
                                    JSONObject row = object.getJSONObject(key);
                                    System.out.println(row);

                                    Iterator<?> keys2 = row.keys();

                                    while (keys2.hasNext()) {

                                        String key2 = (String) keys2.next();
                                        JSONObject res = row.getJSONObject(key2);
                                        Feed feed = new Feed();

                                        feed.setTitle(res.get("title").toString());
                                        feed.setContent(res.get("content").toString());
                                        feed.setDate(new Date(Long.parseLong((res.get("date")).toString())));
                                        feed.setClassId(key);
                                        feed.setId(key2);
                                        feedData.add(feed);
                                    }
                                }


                                // specify an adapter (see also next example)
                                FeedAdapter mAdapter = new FeedAdapter(feedData);
                                mAdapter.setAcc_type(getArguments().getString("ACC_TYPE"));
                                mAdapter.setAuthToken(getArguments().getString("AUTH_ID"));
                                mAdapter.setSchoolId(getArguments().getString("SCHOOL_ID"));
                                mRecyclerView.setAdapter(mAdapter);

                            }
                            catch (JSONException error) {
                                System.out.println("json error feed");
                            }
                        }
                    }
                });

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
