package edugate.com.edugate;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import edugate.com.edugate.Adapter.ReportAdapter;
import edugate.com.edugate.Object.Mark;
import edugate.com.edugate.Object.Report;


public class ReportActivity extends AppCompatActivity {

    private String schoolId;
    private String studentId;
    private String authToken;
    private String accType;
    private String subjectId;

    private RecyclerView mRecyclerView;
    private ReportAdapter mAdapter;
    private ArrayList<Report> reportData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(this.getResources().getColor(R.color.colorAccent));
        }

        setTitle(R.string.report);

        Intent intent = getIntent();
        schoolId = intent.getStringExtra("SCHOOL_ID");
        authToken = intent.getStringExtra("AUTH_ID");
        accType = intent.getStringExtra("ACC_TYPE");
        studentId = intent.getStringExtra("STUDENT_ID");
        subjectId = intent.getStringExtra("SUBJECT_ID");


        System.out.println(studentId);

        setContentView(R.layout.activity_report);

        mRecyclerView = (RecyclerView) findViewById(R.id.ReportRecyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle args = new Bundle();
        args.putString("SCHOOL_ID", schoolId);
        args.putString("STUDENT_ID", studentId);
        args.putString("AUTH_ID", authToken);
        args.putString("ACC_TYPE", accType);

        FloatingActionButton addButton = (FloatingActionButton) findViewById(R.id.fab);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(view.getContext(), AddReportActivity.class);
                intent.putExtra("SCHOOL_ID", schoolId);
                intent.putExtra("AUTH_ID", authToken);
                intent.putExtra("ACC_TYPE", accType);
                intent.putExtra("STUDENT_ID", studentId);
                intent.putExtra("SUBJECT_ID", subjectId);

                view.getContext().startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        menu.add("Logout");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getTitle() == "Logout") {
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.clear();
            editor.commit();

            Intent intent = new Intent(getApplication(), LoginActivity.class);

            startActivity(intent);
            finish();

        }
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;


        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        getBook();
    }

    public void getBook() {


        mAdapter = new ReportAdapter(reportData);
        mRecyclerView.setAdapter(mAdapter);

        System.out.println("Get student reports");
        System.out.println(reportData);
        reportData.clear();

        System.out.println(getString(R.string.main_api_url) + "/book?book_id=" + studentId);

        Ion.with(this).load(getString(R.string.main_api_url) + "/book?book_id=" + studentId)
                .setHeader("Authentification", authToken)
                .setHeader("School", schoolId)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                if (e == null) {
                    System.out.println(result);
                    try {
                        JSONObject object = new JSONObject(result);
                        Iterator<?> keys = object.keys();
                        Gson gson = new Gson();


                        while (keys.hasNext()) {
                            String key = (String) keys.next();
                            System.out.println("Get key: " + object.get(key));

                            if (object.get(key) instanceof JSONObject && key.equals("comments")) {

                                object = (JSONObject) (object.get(key));
                                keys = object.keys();
                                System.out.println("Object: " + object);
                                while (keys.hasNext()) {
                                    key = (String) keys.next();

                                    if (object.get(key) != null) {

                                        if (object.get(key) instanceof JSONObject) {

                                            if (((JSONObject) object.get(key)).get("teacher_id").toString().equals(authToken))
                                            {
                                                Report report = new Report();

                                                report.setTitle("Mot");
                                                report.setEditable(true);
                                                report.setContent(((JSONObject) object.get(key)).get("content").toString());
                                                report.setId(key);
                                                report.setBookId(studentId);
                                                report.setDate(new Date());
                                                reportData.add(report);
                                            }

                                        }
                                    }
                                }

                            }
                        }

                        mAdapter = new ReportAdapter(reportData);
                        mAdapter.setSchoolId(schoolId);
                        mAdapter.setAuthToken(authToken);
                        mRecyclerView.setAdapter(mAdapter);
                    }
                    catch (JSONException error) {
                        System.out.println("json error");
                    }

                }
            }
        });

    }
}