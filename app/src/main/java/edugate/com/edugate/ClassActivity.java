package edugate.com.edugate;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import edugate.com.edugate.Adapter.ClassAdapter;
import edugate.com.edugate.Object.Class;


public class ClassActivity extends AppCompatActivity
{

    private String          schoolId;
    private String          subjectId;
    private String          authToken;
    private String          accType;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private ArrayList<Class> classData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(this.getResources().getColor(R.color.colorAccent));
        }

        setTitle(R.string.classes);

        Intent intent   = getIntent();
        schoolId        = intent.getStringExtra("SCHOOL_ID");

        authToken       = intent.getStringExtra("AUTH_ID");
        accType         = intent.getStringExtra("ACC_TYPE");
        subjectId         = intent.getStringExtra("SUBJECT_ID");

        setContentView(R.layout.activity_class);

        mRecyclerView = (RecyclerView)findViewById(R.id.ClassRecyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle args = new Bundle();
        args.putString("SCHOOL_ID", schoolId);
        args.putString("SUBJECT_ID", subjectId);
        args.putString("AUTH_ID", authToken);
        args.putString("ACC_TYPE", accType);

        Ion.with(this).load(getString(R.string.main_api_url) + "/subject?subject_id=" + subjectId)
                .setHeader("Authentification", authToken)
                .setHeader("School", schoolId)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                if (e == null) {
                    System.out.println(result);

                    try {
                        JSONObject object = new JSONObject(result);
                        Iterator<?> keys = object.keys();
                        Gson gson = new Gson();


                        while (keys.hasNext()) {
                            String key = (String) keys.next();
                            if (object.get(key) instanceof JSONObject && key.equals("class_list")) {
                                System.out.println(key);

                                object = (JSONObject) (object.get(key));
                                keys = object.keys();

                                while (keys.hasNext()) {
                                    key = (String) keys.next();
                                    if (object.get(key) instanceof JSONObject) {
                                        Class classObj = new Class();
                                        classObj.setId(key);
                                            classObj.setName(((JSONObject) object.get(key)).get("name").toString());
                                        classData.add(classObj);
                                    }
                                }

                            }
                        }

                        if (mAdapter == null) {
                            // specify an adapter (see also next example)
                            mAdapter = new ClassAdapter(classData);
                            mRecyclerView.setAdapter(mAdapter);
                        }
                    } catch (JSONException error) {
                        System.out.println("json error");
                    }

                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        menu.add("Logout");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {



        if (item.getTitle() == "Logout")
        {
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.clear();
            editor.commit();

            Intent intent   = new Intent(getApplication(), LoginActivity.class);

            startActivity(intent);
            finish();

        }
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;


        }


        return super.onOptionsItemSelected(item);
    }
}
