package edugate.com.edugate;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import edugate.com.edugate.Object.Mark;

public class DetentionDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Mark mark = new Mark();
        mark.setName(getIntent().getStringExtra("MARK_NAME"));
        mark.setMark(getIntent().getStringExtra("MARK_VALUE"));
        mark.setId(getIntent().getStringExtra("MARK_ID"));


        setContentView(R.layout.activity_mark_detail);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        TextView markValue = (TextView) findViewById(R.id.markValue);
        TextView markName = (TextView) findViewById(R.id.markName);

        markValue.setText(mark.getMark());
        markName.setText(mark.getName());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;


        }

        return super.onOptionsItemSelected(item);
    }
}
