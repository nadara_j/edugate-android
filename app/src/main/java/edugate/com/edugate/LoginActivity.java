package edugate.com.edugate;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;


public class LoginActivity extends ActionBarActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final Button LoginButton = (Button)findViewById(R.id.login_button);
        final Button LoginStudentButton = (Button)findViewById(R.id.login_student_button);
        final Button LoginTeacherButton = (Button)findViewById(R.id.login_teacher_button);
        final ProgressBar progressBar = (ProgressBar)findViewById(R.id.progressbar_login);
        final CheckBox remember_login = (CheckBox)findViewById(R.id.remember_login);
        final EditText login_input = (EditText) findViewById(R.id.login_input);
        final EditText password_input = (EditText) findViewById(R.id.password_input);

        progressBar.setVisibility(View.INVISIBLE);
        LoginButton.setOnClickListener(this);
        LoginStudentButton.setOnClickListener(this);
        LoginTeacherButton.setOnClickListener(this);

        final String username = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("USERNAME", "EMPTY");
        final String password = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("PASSWORD", "EMPTY");

        final Intent intent = new Intent(getApplication(), MainActivity.class);



        if (!username.equals("EMPTY") && !password.equals("EMPTY")) {
            progressBar.setVisibility(View.VISIBLE);
            LoginButton.setVisibility(View.INVISIBLE);
            remember_login.setVisibility(View.INVISIBLE);
            login_input.setVisibility(View.INVISIBLE);
            password_input.setVisibility(View.INVISIBLE);

            Log.d("LoginActivity", "email " + username);
            Log.d("LoginActivity", "password " + password);


            Ion.with(getApplication())
                    .load("POST", getString(R.string.main_api_url) + "/login")
                    .setBodyParameter("email", username)
                    .setBodyParameter("password", password)
                    .asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, final String result) {
                            if (e != null) {
                                AlertDialog.Builder alert = new AlertDialog.Builder(LoginActivity.this);
                                        alert.setTitle("Error");
                                        alert.setMessage("Connexion is lost");
                                        alert.setNeutralButton("Close", null);
                                        alert.show();

                            }
                            else {
                                try {
                                    System.out.print("TRY LOGIN !!!!!!!!!!!!");
                                    JSONObject object = new JSONObject(result);
                                    Iterator<?> keys = object.keys();
                                    Gson gson = new Gson();

                                    System.out.print("TRY LOGIN !!!!!!!!!!!!");

                                    while (keys.hasNext()) {
                                        String key = (String) keys.next();
                                        System.out.print(object.get(key));

                                        //if (object.has("email")) {
                                            progressBar.setVisibility(View.INVISIBLE);

                                            String schoolId = object.get("school_id").toString();
                                            String authId = object.get("authentificationToken").toString();
                                            String accType = object.get("type").toString();

                                            if (accType.equals("Parent")) {
                                                System.out.print("TRY LOGIN PARENT !!!!!!!!!!!!");
                                                JSONObject obj = (JSONObject) object.get("student_list");
                                                Iterator<?> keys2 = obj.keys();

                                                String key2 = (String) keys2.next();

                                                System.out.print(key2);
                                                authId = key2.toString();
                                            }

                                            intent.putExtra("AUTH_ID", authId);
                                            intent.putExtra("SCHOOL_ID", schoolId);

                                            intent.putExtra("ACC_TYPE", accType);

                                            System.out.println("AuthId Login: " + authId);

                                            Ion.with(getApplication())
                                                    .load("GET", getString(R.string.main_api_url) + "/student?student_id=" + authId)
                                                    .setHeader("School", schoolId)
                                                    .asJsonObject()
                                                    .setCallback(new FutureCallback<JsonObject>() {
                                                        @Override
                                                        public void onCompleted(Exception e, final JsonObject result2) {
                                                            String classId = "";
                                                            if (result2.has("class_id"))
                                                                classId = result2.get("class_id").getAsString();

                                                            System.out.println("Class Id Result: " + result2);
                                                            System.out.println("Class Id: " + classId);

                                                            intent.putExtra("CLASS_ID", classId);
                                                            startActivity(intent);
                                                            finish();
                                                        }
                                                    });
/*
                                        } else {

                                            LoginButton.setVisibility(View.VISIBLE);
                                            remember_login.setVisibility(View.VISIBLE);
                                            login_input.setVisibility(View.VISIBLE);
                                            password_input.setVisibility(View.VISIBLE);

                                            AlertDialog.Builder builder = new AlertDialog.Builder(getBaseContext());
                                            builder.setTitle("Error");

                                            if (object.has("error")) {
                                                builder.setMessage(object.get("error").toString());

                                            } else
                                                builder.setMessage("Unknown Error");

                                            builder.setNeutralButton("Close", null).show();
                                        }
                                        */
                                    }

                                }
                                catch (JSONException error) {
                                    System.out.println("json error");
                                }


                            }
                        }
                    });
        }
        else
        {
            System.out.println("empty");
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(final View v) {
        final Intent intent = new Intent(getApplication(), MainActivity.class);

        if (v.getId() == R.id.login_button) {
            final Button LoginButton = (Button)findViewById(R.id.login_button);
            final CheckBox remember_login = (CheckBox)findViewById(R.id.remember_login);
            final EditText login_input = (EditText) findViewById(R.id.login_input);
            final EditText password_input = (EditText) findViewById(R.id.password_input);
            final Context current_context = v.getContext();


            Ion.with(v.getContext())
                    .load("POST", getString(R.string.main_api_url) + "/login")
                    .setBodyParameter("email", login_input.getText().toString())
                    .setBodyParameter("password", password_input.getText().toString())
                    .asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, final String result) {
                            if (e != null) {
                                AlertDialog.Builder dialog =  new AlertDialog.Builder(LoginActivity.this);

                                dialog.setTitle("Error");
                                dialog.setMessage("Connexion is lost");
                                dialog.setNeutralButton("Close", null);
                                dialog.show();
                            }
                            else {
                                //if (result.has("email")) {


                                    if (remember_login.isChecked()) {
                                        PreferenceManager.
                                                getDefaultSharedPreferences(v.getContext())
                                                .edit()
                                                .putString("USERNAME", login_input.getText().toString())
                                                .putString("PASSWORD", password_input.getText().toString())
                                                .commit();
                                    }






                                try {
                                    System.out.println(result);
                                    JSONObject object = new JSONObject(result);
                                    Iterator<?> keys = object.keys();
                                    Gson gson = new Gson();

                                    System.out.println("TRY LOGIN !!!!!!!!!!!!");

                                    while (keys.hasNext()) {
                                        System.out.println("TRY LOGIN !!!!!!!!!!!!2");
                                        String key = (String) keys.next();

                                        //if (object.has("email")) {
                                        System.out.println("TRY LOGIN !!!!!!!!!!!!3");
                                        System.out.println(object);

                                        String schoolId = (String) object.get("school_id");
                                        String authId = (String) object.get("authentificationToken");
                                        String accType = (String) object.get("type");
                                        System.out.println("TRY LOGIN !!!!!!!!!!!!4");
                                        if (accType.equals("Parent")) {
                                            System.out.print("TRY LOGIN PARENT !!!!!!!!!!!!");
                                            JSONObject obj = (JSONObject) object.get("student_list");
                                            Iterator<?> keys2 = obj.keys();

                                            String key2 = (String) keys2.next();

                                            System.out.print(key2);
                                            authId = key2.toString();
                                        }

                                        System.out.println("AuthId Login: " + authId);

                                        Ion.with(getApplication())
                                                .load("GET", getString(R.string.main_api_url) + "/student?student_id=" + authId)
                                                .setHeader("School", schoolId)
                                                .asJsonObject()
                                                .setCallback(new FutureCallback<JsonObject>() {
                                                    @Override
                                                    public void onCompleted(Exception e, final JsonObject result2) {
                                                        String classId  = "";
                                                        if (result2.has("class_id"))
                                                            classId  = result2.get("class_id").getAsString();

                                                        System.out.println("Class Id Result: " + result2);
                                                        System.out.println("Class Id: " + classId);

                                                        intent.putExtra("CLASS_ID", classId);
                                                        startActivity(intent);
                                                        finish();
                                                    }
                                                });

                                        intent.putExtra("AUTH_ID", authId);
                                        intent.putExtra("SCHOOL_ID", schoolId);

                                        intent.putExtra("ACC_TYPE", accType);

                                        System.out.println("AuthId Login: " + authId);

                                        Ion.with(getApplication())
                                                .load("GET", getString(R.string.main_api_url) + "/student?student_id=" + authId)
                                                .setHeader("School", schoolId)
                                                .asJsonObject()
                                                .setCallback(new FutureCallback<JsonObject>() {
                                                    @Override
                                                    public void onCompleted(Exception e, final JsonObject result2) {
                                                        String classId = "";
                                                        if (result2.has("class_id"))
                                                            classId = result2.get("class_id").getAsString();

                                                        System.out.println("Class Id Result: " + result2);
                                                        System.out.println("Class Id: " + classId);

                                                        intent.putExtra("CLASS_ID", classId);
                                                        startActivity(intent);
                                                        finish();
                                                    }
                                                });
/*
                                        } else {

                                            LoginButton.setVisibility(View.VISIBLE);
                                            remember_login.setVisibility(View.VISIBLE);
                                            login_input.setVisibility(View.VISIBLE);
                                            password_input.setVisibility(View.VISIBLE);

                                            AlertDialog.Builder builder = new AlertDialog.Builder(getBaseContext());
                                            builder.setTitle("Error");

                                            if (object.has("error")) {
                                                builder.setMessage(object.get("error").toString());

                                            } else
                                                builder.setMessage("Unknown Error");

                                            builder.setNeutralButton("Close", null).show();
                                        }
                                        */
                                    }





                                }
                                catch (JSONException error) {
                                    System.out.println("json error");
                                }



                               /* }
                                else {

                                    LoginButton.setVisibility(View.VISIBLE);
                                    remember_login.setVisibility(View.VISIBLE);
                                    login_input.setVisibility(View.VISIBLE);
                                    password_input.setVisibility(View.VISIBLE);

                                    AlertDialog.Builder builder = new AlertDialog.Builder(current_context)
                                            .setTitle("Error");

                                    if (result.has("error"))
                                        builder.setMessage(result.get("error").getAsString());
                                    else
                                        builder.setMessage("Unknown Error");

                                    builder.setNeutralButton("Close", null).show();
                                }
                                */
                            }

                        }
                    });
        }
        if (v.getId() == R.id.login_student_button) {
            final Button LoginButton = (Button)findViewById(R.id.login_student_button);
            final CheckBox remember_login = (CheckBox)findViewById(R.id.remember_login);
            final EditText login_input = (EditText) findViewById(R.id.login_input);
            final EditText password_input = (EditText) findViewById(R.id.password_input);
            final Context current_context = v.getContext();

            System.out.println("LOGIN STUDENT 1");
            Ion.with(v.getContext())
                    .load("POST", getString(R.string.main_api_url) + "/login")
                    .setBodyParameter("email",  "jzepitech@gmail.com")
                    .setBodyParameter("password", "9v841jor")
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, final JsonObject result) {
                            if (e != null) {
                                AlertDialog.Builder dialog =  new AlertDialog.Builder(LoginActivity.this);

                                dialog.setTitle("Error");
                                dialog.setMessage("Connexion is lost");
                                dialog.setNeutralButton("Close", null);
                                dialog.show();
                            }
                            else {
                                if (result.has("email")) {
                                    if (remember_login.isChecked()) {
                                        PreferenceManager.
                                                getDefaultSharedPreferences(v.getContext())
                                                .edit()
                                                .putString("USERNAME", "jzepitech@gmail.com")
                                                .putString("PASSWORD", "9v841jor")
                                                .commit();
                                    }


                                    String schoolId     = result.get("school_id").getAsString();
                                    String authId       = result.get("authentificationToken").getAsString();
                                    String accType      = result.get("type").getAsString();


                                    intent.putExtra("AUTH_ID", authId);
                                    intent.putExtra("SCHOOL_ID", schoolId);

                                    intent.putExtra("ACC_TYPE", accType);

                                    System.out.println("AuthId Login: " + authId);

                                    Ion.with(getApplication())
                                            .load("GET", getString(R.string.main_api_url) + "/student?student_id=" + authId)
                                            .setHeader("School", schoolId)
                                            .asJsonObject()
                                            .setCallback(new FutureCallback<JsonObject>() {
                                                @Override
                                                public void onCompleted(Exception e, final JsonObject result2) {
                                                    String classId  = "";
                                                    if (result2.has("class_id"))
                                                        classId  = result2.get("class_id").getAsString();

                                                    System.out.println("Class Id Result: " + result2);
                                                    System.out.println("Class Id: " + classId);

                                                    intent.putExtra("CLASS_ID", classId);
                                                    startActivity(intent);
                                                    finish();
                                                }
                                            });



                                }
                                else {

                                    LoginButton.setVisibility(View.VISIBLE);
                                    remember_login.setVisibility(View.VISIBLE);
                                    login_input.setVisibility(View.VISIBLE);
                                    password_input.setVisibility(View.VISIBLE);

                                    AlertDialog.Builder builder = new AlertDialog.Builder(current_context)
                                            .setTitle("Error");

                                    if (result.has("error"))
                                        builder.setMessage(result.get("error").getAsString());
                                    else
                                        builder.setMessage("Unknown Error");

                                    builder.setNeutralButton("Close", null).show();
                                }
                            }

                        }
                    });
        }
        if (v.getId() == R.id.login_teacher_button) {
            final Button LoginButton = (Button)findViewById(R.id.login_teacher_button);
            final CheckBox remember_login = (CheckBox)findViewById(R.id.remember_login);
            final EditText login_input = (EditText) findViewById(R.id.login_input);
            final EditText password_input = (EditText) findViewById(R.id.password_input);
            final Context current_context = v.getContext();


            Ion.with(v.getContext())
                    .load("POST", getString(R.string.main_api_url) + "/login")
                    .setBodyParameter("email",  "michel@epitech.eu")
                    .setBodyParameter("password", "nmdq85mi")
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, final JsonObject result) {
                            if (e != null) {
                                AlertDialog.Builder dialog =  new AlertDialog.Builder(LoginActivity.this);

                                dialog.setTitle("Error");
                                dialog.setMessage("Connexion is lost");
                                dialog.setNeutralButton("Close", null);
                                dialog.show();
                            }
                            else {
                                if (result.has("email")) {
                                    if (remember_login.isChecked()) {
                                        PreferenceManager.
                                                getDefaultSharedPreferences(v.getContext())
                                                .edit()
                                                .putString("USERNAME", "michel@epitech.eu")
                                                .putString("PASSWORD", "nmdq85mi")
                                                .commit();
                                    }

                                    String schoolId     = result.get("school_id").getAsString();
                                    String authId       = result.get("authentificationToken").getAsString();
                                    String accType      = result.get("type").getAsString();


                                    intent.putExtra("AUTH_ID", authId);
                                    intent.putExtra("SCHOOL_ID", schoolId);

                                    intent.putExtra("ACC_TYPE", accType);

                                    System.out.println("AuthId Login: " + authId);

                                    Ion.with(getApplication())
                                            .load("GET", getString(R.string.main_api_url) + "/student?student_id=" + authId)
                                            .setHeader("School", schoolId)
                                            .asJsonObject()
                                            .setCallback(new FutureCallback<JsonObject>() {
                                                @Override
                                                public void onCompleted(Exception e, final JsonObject result2) {
                                                    String classId  = "";
                                                    if (result2.has("class_id"))
                                                        classId  = result2.get("class_id").getAsString();

                                                    System.out.println("Class Id Result: " + result2);
                                                    System.out.println("Class Id: " + classId);

                                                    intent.putExtra("CLASS_ID", classId);
                                                    startActivity(intent);
                                                    finish();
                                                }
                                            });
                                }
                                else {

                                    LoginButton.setVisibility(View.VISIBLE);
                                    remember_login.setVisibility(View.VISIBLE);
                                    login_input.setVisibility(View.VISIBLE);
                                    password_input.setVisibility(View.VISIBLE);

                                    AlertDialog.Builder builder = new AlertDialog.Builder(current_context)
                                            .setTitle("Error");

                                    if (result.has("error"))
                                        builder.setMessage(result.get("error").getAsString());
                                    else
                                        builder.setMessage("Unknown Error");

                                    builder.setNeutralButton("Close", null).show();
                                }
                            }

                        }
                    });
        }
    }
}
