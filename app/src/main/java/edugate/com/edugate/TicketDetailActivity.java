package edugate.com.edugate;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.Date;

import edugate.com.edugate.Adapter.MessageAdapter;
import edugate.com.edugate.Object.Message;

public class TicketDetailActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private ArrayList<Message> messageData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_ticket_detail);

        mRecyclerView = (RecyclerView) findViewById(R.id.MessageRecyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final String authId = getIntent().getStringExtra("AUTH_ID");
        final String schoolId = getIntent().getStringExtra("SCHOOL_ID");
        final String ticketId = getIntent().getStringExtra("TICKET_ID");

        Log.wtf("url = ", getString(R.string.main_api_url) + "/ticket?ticket_id=" + ticketId);
        System.out.println(authId);
        System.out.println(schoolId);
        System.out.println(ticketId);



        Ion.with(this)
                .load(getString(R.string.main_api_url) + "/ticket?ticket_id=" + ticketId)
                .setHeader("Authentification", authId)
                .setHeader("School", schoolId)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (e == null) {
                            System.out.println("result ticket detail");
                            System.out.println(result);

                            JsonArray conversation = result.get("conversation").getAsJsonArray();

                            for (int i = 0; i < conversation.size(); i++) {
                                Message message = new Message();
                                message.setContent(conversation.get(i).getAsJsonObject().get("content").getAsString());

                                long timestamp2 = Long.parseLong(conversation.get(i).getAsJsonObject().get("date").getAsString());
                                message.setDate(new Date(timestamp2));
                                message.setSender(conversation.get(i).getAsJsonObject().get("user_name").getAsString());
                                if(conversation.get(i).getAsJsonObject().get("user_id").getAsString().equals(authId))
                                    message.setOwner(true);
                                else
                                    message.setOwner(false);

                                messageData.add(message);
                            }

                            mAdapter = new MessageAdapter(messageData);
                            mRecyclerView.setAdapter(mAdapter);
                        }
                    }
                });

        final EditText editMessage = (EditText)findViewById(R.id.editText);

        Button sendButton = (Button)findViewById(R.id.sendButton);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                Ion.with(view.getContext())
                        .load("PUT", getString(R.string.main_api_url) + "/ticket/answer?ticket_id=" + ticketId)
                        .setHeader("Authentification", authId)
                        .setHeader("School", schoolId)
                        .setBodyParameter("content", editMessage.getText().toString())
                        .asString()
                        .setCallback(new FutureCallback<String>() {
                            @Override
                            public void onCompleted(Exception e, String result) {
                                if (e == null) {
                                    editMessage.setText("");

                                    Ion.with(view.getContext())
                                            .load(getString(R.string.main_api_url) + "/ticket?ticket_id=" + ticketId)
                                            .setHeader("Authentification", authId)
                                            .setHeader("School", schoolId)
                                            .asJsonObject()
                                            .setCallback(new FutureCallback<JsonObject>() {
                                                @Override
                                                public void onCompleted(Exception e, JsonObject result) {
                                                    if (e == null) {
                                                        System.out.println("result ticket detail");
                                                        System.out.println(result);

                                                        JsonArray conversation = result.get("conversation").getAsJsonArray();

                                                        messageData.clear();
                                                        for (int i = 0; i < conversation.size(); i++) {
                                                            Message message = new Message();
                                                            message.setContent(conversation.get(i).getAsJsonObject().get("content").getAsString());

                                                            long timestamp2 = Long.parseLong(conversation.get(i).getAsJsonObject().get("date").getAsString());
                                                            message.setDate(new Date(timestamp2));
                                                            message.setSender(conversation.get(i).getAsJsonObject().get("user_name").getAsString());
                                                            messageData.add(message);
                                                        }

                                                        mAdapter = new MessageAdapter(messageData);
                                                        mRecyclerView.setAdapter(mAdapter);
                                                    }
                                                }
                                            });
                                }
                            }
                        });


            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;


        }

        return super.onOptionsItemSelected(item);
    }
}
