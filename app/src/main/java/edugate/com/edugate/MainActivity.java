package edugate.com.edugate;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.transition.Slide;
import android.transition.Transition;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.Random;

import edugate.com.edugate.Fragment.CalendarFragment;
import edugate.com.edugate.Fragment.DetentionFragment;
import edugate.com.edugate.Fragment.FeedFragment;
import edugate.com.edugate.Fragment.FeedFragmentTeacher;
import edugate.com.edugate.Fragment.HomeworkFragment;
import edugate.com.edugate.Fragment.MarkFragment;
import edugate.com.edugate.Fragment.ReportFragment;
import edugate.com.edugate.Fragment.TakeRollFragment;
import edugate.com.edugate.Fragment.TeacherReportFragment;
import edugate.com.edugate.Fragment.TicketsFragment;
import edugate.com.edugate.Service.NotificationService;

public class MainActivity extends AppCompatActivity
    implements FeedFragment.OnFragmentInteractionListener,
        FeedFragmentTeacher.OnFragmentInteractionListener,
        ReportFragment.OnFragmentInteractionListener,
        MarkFragment.OnFragmentInteractionListener,
        CalendarFragment.OnFragmentInteractionListener,
        TakeRollFragment.OnFragmentInteractionListener,
        DetentionFragment.OnFragmentInteractionListener,
        HomeworkFragment.OnFragmentInteractionListener,
        TicketsFragment.OnFragmentInteractionListener,
        TeacherReportFragment.OnFragmentInteractionListener
    {


        /**
         * The {@link ViewPager} that will host the section contents.
         */
        ViewPager               mViewPager;
        private DrawerLayout    mDrawerLayout;
        private String          schoolId;
        private String          classId;
        private String          authToken;
        private String          accType;
        private String          planingId;


        @Override
        protected void onCreate(Bundle savedInstanceState) {


            super.onCreate(savedInstanceState);



            Intent intent   = getIntent();

            if (intent == null) {
                final Bundle extra = getIntent().getExtras();
                if (extra != null) {
                    accType         = extra.getString("ACC_TYPE");

                }
            }
            else {
                schoolId        = intent.getStringExtra("SCHOOL_ID");
                classId         = intent.getStringExtra("CLASS_ID");
                authToken       = intent.getStringExtra("AUTH_ID");
                accType         = intent.getStringExtra("ACC_TYPE");
            }


            schoolId        = intent.getStringExtra("SCHOOL_ID");
            classId         = intent.getStringExtra("CLASS_ID");
            authToken       = intent.getStringExtra("AUTH_ID");
            accType         = intent.getStringExtra("ACC_TYPE");

            System.out.println("try main 1");

            if (accType.equals("Student") || accType.equals("Parent"))
                setContentView(R.layout.activity_main_student);
            else if (accType.equals("Teacher")) {
                setContentView(R.layout.activity_main_teacher);
                Ion.with(this)
                        .load(getString(R.string.main_api_url) + "/teacher?teacher_id=" + authToken)
                        .setHeader("School", schoolId)
                        .setHeader("Authentification", authToken)
                        .asString()
                        .setCallback(new FutureCallback<String>() {
                            @Override
                            public void onCompleted(Exception e, String result) {
                                if (e == null) {
                                    try {
                                        JSONObject object = new JSONObject(result);
                                        Iterator<?> keys = object.keys();
                                        Gson gson = new Gson();


                                        while (keys.hasNext()) {
                                            String key = (String) keys.next();
                                            if (key.equals("planning_id")) {
                                                planingId = object.get(key).toString();
                                            }
                                        }
                                    } catch (JSONException ex) {
                                        System.out.println("execption");
                                    }
                                }
                            }
                        });

            }
            else
                setContentView(R.layout.activity_main);



            setTitle(getString(R.string.home));

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            final ActionBar ab = getSupportActionBar();
            ab.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
            ab.setDisplayHomeAsUpEnabled(true);



            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

            NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
            if (navigationView != null) {
                setupDrawerContent(navigationView);
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Transition exitTrans = new Explode();
                getWindow().setExitTransition(exitTrans);

                Transition reenterTrans = new Slide();
                getWindow().setReenterTransition(reenterTrans);
            }

            Bundle args = new Bundle();
            args.putString("SCHOOL_ID", schoolId);
            args.putString("CLASS_ID", classId);
            args.putString("AUTH_ID", authToken);
            args.putString("ACC_TYPE", accType);
            args.putString("TRANSACTION_TYPE", "FEED");

            System.out.println("try main 2");


            if (accType.equals("Student")  || accType.equals("Parent")) {
                FeedFragment fragment = new FeedFragment();
                fragment.setArguments(args);
                android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.nav_contentframe, fragment);
                fragmentTransaction.commit();
            }
            else if (accType.equals("Teacher"))
            {
                FeedFragmentTeacher fragment = new FeedFragmentTeacher();
                fragment.setArguments(args);
                android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.nav_contentframe, fragment);
                fragmentTransaction.commit();
            }




            /* Notification service awaken every minutes */
            int timeForAlarm= 60000;

            AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
            Intent notifIntent = new Intent(this, NotificationService.class);
            System.out.println("authID notif " + authToken);
            System.out.println("schoolID " + schoolId);

            notifIntent.putExtra("authID", authToken);
            notifIntent.putExtra("schoolID", schoolId);
            notifIntent.putExtra("accType", accType);
            notifIntent.putExtra("classID", classId);

            PendingIntent pendingIntent = PendingIntent.getService(this, 0, notifIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            try {
                alarmManager.cancel(pendingIntent);
            }
            catch (Exception e) {
                Log.d("error", "notif");
            }



            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + timeForAlarm, timeForAlarm, pendingIntent);
        }


        @Override
        public boolean onCreateOptionsMenu(Menu menu) {

            menu.add("Logout");
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {



            if (item.getTitle() == "Logout")
            {
                SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.clear();
                editor.commit();

                Intent intent   = new Intent(getApplication(), LoginActivity.class);

                startActivity(intent);
                finish();

            }
            switch (item.getItemId()) {
                case android.R.id.home:
                    mDrawerLayout.openDrawer(GravityCompat.START);
                    return true;

            }







            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.

        /*
        if (item.getTitle().toString().equals("Logout")) {
            System.out.println("Logout Clicked");
            PreferenceManager.
                    getDefaultSharedPreferences(this)
                    .edit()
                    .remove("USERNAME")
                    .remove("PASSWORD")
                    .commit();
            Intent intent = new Intent(this, LoginActivity.class);
            this.startActivity(intent);
            finish();
        }*/
            return super.onOptionsItemSelected(item);
        }

        private void setupDrawerContent(NavigationView navigationView) {
            navigationView.setNavigationItemSelectedListener(
                    new NavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(MenuItem menuItem) {

                            Bundle args = new Bundle();
                            args.putString("SCHOOL_ID", schoolId);
                            args.putString("CLASS_ID", classId);
                            args.putString("AUTH_ID", authToken);
                            args.putString("ACC_TYPE", accType);

                            if (accType.equals("Teacher")) {
                                args.putString("PLANNING_ID", planingId);
                            }


                            menuItem.setChecked(true);
                            mDrawerLayout.closeDrawers();
                            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                            switch (menuItem.getItemId()) {
                                case R.id.navigation_item_home:
                                    System.out.println("try main 3");
                                    setTitle(getString(R.string.home));

                                    if (accType.equals("Student")  || accType.equals("Parent")) {
                                        FeedFragment fragment = new FeedFragment();
                                        fragment.setArguments(args);
                                        fragmentTransaction.replace(R.id.nav_contentframe,fragment);
                                        fragmentTransaction.commit();
                                    }
                                    else if (accType.equals("Teacher"))
                                    {
                                        FeedFragmentTeacher fragment = new FeedFragmentTeacher();
                                        fragment.setArguments(args);
                                        fragmentTransaction.replace(R.id.nav_contentframe,fragment);
                                        fragmentTransaction.commit();
                                    }
                                    return true;
                                case R.id.navigation_item_report:
                                    ReportFragment fragmentReport = new ReportFragment();
                                    fragmentReport.setArguments(args);
                                    setTitle(getString(R.string.report));
                                    fragmentTransaction.replace(R.id.nav_contentframe,fragmentReport);
                                    fragmentTransaction.commit();
                                    return true;
                                case R.id.navigation_item_marks:
                                    MarkFragment fragmentMark = new MarkFragment();
                                    fragmentMark.setArguments(args);
                                    setTitle(getString(R.string.marks));
                                    fragmentTransaction.replace(R.id.nav_contentframe,fragmentMark);
                                    fragmentTransaction.commit();
                                    return true;
                                case R.id.navigation_item_calendar:
                                    CalendarFragment calendarFragment = new CalendarFragment();
                                    calendarFragment.setArguments(args);
                                    setTitle(getString(R.string.calendar));
                                    fragmentTransaction.replace(R.id.nav_contentframe, calendarFragment);
                                    fragmentTransaction.commit();
                                    return true;
                                case R.id.navigation_item_roll:
                                    TakeRollFragment rollFragment = new TakeRollFragment();
                                    rollFragment.setArguments(args);
                                    setTitle(getString(R.string.take_roll));
                                    fragmentTransaction.replace(R.id.nav_contentframe, rollFragment);
                                    fragmentTransaction.commit();
                                    return true;
                                case R.id.navigation_item_report_teacher:
                                    TeacherReportFragment reportFragment = new TeacherReportFragment();
                                    reportFragment.setArguments(args);
                                    setTitle(getString(R.string.report));
                                    fragmentTransaction.replace(R.id.nav_contentframe, reportFragment);
                                    fragmentTransaction.commit();
                                    return true;
                                case R.id.navigation_item_detention:
                                    DetentionFragment detentionFragment = new DetentionFragment();
                                    detentionFragment.setArguments(args);
                                    setTitle(getString(R.string.detention));
                                    fragmentTransaction.replace(R.id.nav_contentframe, detentionFragment);

                                    fragmentTransaction.commit();
                                    return true;
                                case R.id.navigation_item_homework:
                                    HomeworkFragment homeworkFragment = new HomeworkFragment();
                                    homeworkFragment.setArguments(args);
                                    setTitle(getString(R.string.homework));
                                    fragmentTransaction.replace(R.id.nav_contentframe, homeworkFragment);
                                    fragmentTransaction.commit();
                                    return true;
                                case R.id.navigation_item_tickets:
                                    TicketsFragment ticketsFragment = new TicketsFragment();
                                    ticketsFragment.setArguments(args);
                                    setTitle(getString(R.string.tickets));
                                    fragmentTransaction.replace(R.id.nav_contentframe, ticketsFragment);
                                    fragmentTransaction.commit();
                                    return true;
                                default:
                                    return true;
                            }
                        }
                    });
        }


        @Override
        public void onFragmentInteraction(Uri uri) {

        }
    }
