package edugate.com.edugate;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;


import edugate.com.edugate.Adapter.SubjectAdapter;
import edugate.com.edugate.Object.*;
import edugate.com.edugate.Object.Class;


public class SubjectActivity extends AppCompatActivity {

    private String schoolId;
    private String studentId;
    private String authToken;
    private String accType;
    private String subjectId;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private ArrayList<Subject> subjectData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(this.getResources().getColor(R.color.colorAccent));
        }

        setTitle(R.string.subjects);

        Intent intent   = getIntent();
        schoolId        = intent.getStringExtra("SCHOOL_ID");

        authToken       = intent.getStringExtra("AUTH_ID");
        accType         = intent.getStringExtra("ACC_TYPE");
        subjectId         = intent.getStringExtra("SUBJECT_ID");

        setContentView(R.layout.activity_subject);

        mRecyclerView = (RecyclerView)findViewById(R.id.SubjectRecyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle args = new Bundle();
        args.putString("SCHOOL_ID", schoolId);
        args.putString("SUBJECT_ID", subjectId);
        args.putString("AUTH_ID", authToken);
        args.putString("ACC_TYPE", accType);

        System.out.println(authToken);
        System.out.println(schoolId);

        Ion.with(this).load(getString(R.string.main_api_url) + "/teacher?teacher_id=" + authToken)
                .setHeader("Authentification", authToken)
                .setHeader("School", schoolId)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                if (e == null)
                {
                    System.out.println(result);
                    try {
                        JSONObject object = new JSONObject(result);
                        Iterator<?> keys = object.keys();
                        Gson gson = new Gson();


                        while (keys.hasNext()) {
                            String key = (String) keys.next();
                            if (object.get(key) instanceof JSONObject && key.equals("subject_list")) {
                                System.out.println(key);

                                object = (JSONObject)(object.get(key));
                                keys = object.keys();
                                gson = new Gson();
                                while (keys.hasNext()) {
                                    key = (String) keys.next();
                                    if (object.get(key) instanceof JSONObject) {
                                        Subject subject = new Subject();
                                        subject.setId(key);
                                        subject.setName(((JSONObject) object.get(key)).get("subject_name").toString());
                                        subjectData.add(subject);
                                    }
                                }

                            }
                        }

                        mAdapter = new SubjectAdapter(subjectData);
                        mRecyclerView.setAdapter(mAdapter);

                    }
                    catch (JSONException error) {
                        System.out.println("json error");
                    }
                }
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        menu.add("Logout");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getTitle() == "Logout") {
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.clear();
            editor.commit();

            Intent intent = new Intent(getApplication(), LoginActivity.class);

            startActivity(intent);
            finish();

        }
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;


        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        getMarks();
    }

    public void getMarks() {


/*
markData.clear();
        Ion.with(this).load(getString(R.string.main_api_url) + "/book?book_id=" + studentId)
                .setHeader("Authentification", authToken)
                .setHeader("School", schoolId)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                if (e == null) {

                    try {
                        JSONObject object = new JSONObject(result);
                        Iterator<?> keys = object.keys();
                        Gson gson = new Gson();


                        while (keys.hasNext()) {
                            String key = (String) keys.next();
                            if (object.get(key) instanceof JSONObject && key.equals("notes")) {

                                object = (JSONObject) (object.get(key));
                                keys = object.keys();

                                while (keys.hasNext()) {
                                    key = (String) keys.next();

                                    if (object.get(key) != null) {

                                        if (object.get(key) instanceof JSONObject
                                                && authToken.equals(((JSONObject) object.get(key)).get("teacher_id").toString())
                                                && subjectId.equals(((JSONObject) object.get(key)).get("subject_id").toString())) {

                                            Mark mark = new Mark();
                                            mark.setId(key);
                                            mark.setName(((JSONObject) object.get(key)).get("title").toString());
                                            mark.setMark(((JSONObject) object.get(key)).get("value").toString());
                                            markData.add(mark);
                                        }
                                    }
                                }

                            }
                        }

                        mAdapter = new MarkAdapter(markData);
                        mRecyclerView.setAdapter(mAdapter);
                    }
                    catch (JSONException error) {
                        System.out.println("json error");
                    }

                }
            }
        });
        */
    }
}