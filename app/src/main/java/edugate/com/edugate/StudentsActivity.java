package edugate.com.edugate;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import edugate.com.edugate.Adapter.StudentAdapter;
import edugate.com.edugate.Object.Student;


public class StudentsActivity extends AppCompatActivity
{
    private String          schoolId;
    private String          classId;
    private String          authToken;
    private String          accType;
    private String          subjectId;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private ArrayList<Student> studentData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(this.getResources().getColor(R.color.colorAccent));
        }

        Intent intent   = getIntent();
        schoolId        = intent.getStringExtra("SCHOOL_ID");
        subjectId       = intent.getStringExtra("SUBJECT_ID");
        authToken       = intent.getStringExtra("AUTH_ID");
        accType         = intent.getStringExtra("ACC_TYPE");
        classId         = intent.getStringExtra("CLASS_ID");

        setContentView(R.layout.activity_students);

        setTitle(getString(R.string.students));

        mRecyclerView = (RecyclerView)findViewById(R.id.StudentRecyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle args = new Bundle();
        args.putString("SCHOOL_ID", schoolId);
        args.putString("CLASS_ID", classId);
        args.putString("AUTH_ID", authToken);
        args.putString("ACC_TYPE", accType);

        Ion.with(this).load(getString(R.string.main_api_url) + "/class?class_id=" + classId)
                .setHeader("Authentification", authToken)
                .setHeader("School", schoolId)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                if (e == null) {
                    try {
                        JSONObject object = new JSONObject(result);
                        Iterator<?> keys = object.keys();

                        while (keys.hasNext()) {
                            String key = (String) keys.next();
                            if (object.get(key) instanceof JSONObject && key.equals("student_list")) {

                                object = (JSONObject) (object.get(key));
                                keys = object.keys();
                                while (keys.hasNext()) {
                                    key = (String) keys.next();
                                    if (object.get(key) instanceof JSONObject) {
                                        Student student = new Student();
                                        student.setBookId(((JSONObject) object.get(key)).get("book_id").toString());
                                        student.setName(((JSONObject) object.get(key)).get("name").toString());
                                        student.setId(key);
                                        studentData.add(student);
                                    }
                                }

                            }
                        }

                        if (mAdapter == null) {
                            // specify an adapter (see also next example)
                            mAdapter = new StudentAdapter(studentData);
                            mRecyclerView.setAdapter(mAdapter);
                        }
                    } catch (JSONException error) {
                        System.out.println("json error");
                    }

                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        menu.add("Logout");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {



        if (item.getTitle() == "Logout")
        {
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.clear();
            editor.commit();

            Intent intent   = new Intent(getApplication(), LoginActivity.class);

            startActivity(intent);
            finish();

        }
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;


        }


        return super.onOptionsItemSelected(item);
    }
}
