package edugate.com.edugate;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import edugate.com.edugate.Object.Homework;

public class AddReportActivity extends AppCompatActivity {
    private ArrayList<String>   subjectArray    = new ArrayList<>();
    private ArrayList<Homework> homeworkArray   = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_report);


        final String school                     = getIntent().getStringExtra("SCHOOL_ID");
        final String bookId                     = getIntent().getStringExtra("STUDENT_ID");
        final String subject_id                 = getIntent().getStringExtra("SUBJECT_ID");
        final String authId                     = getIntent().getStringExtra("AUTH_ID");

        final Activity backup                   = this;


        setButtonActionWithHomework();


    }

    public void setButtonActionWithHomework() {
        final String school                     = getIntent().getStringExtra("SCHOOL_ID");
        final String bookId                     = getIntent().getStringExtra("STUDENT_ID");
        final String subject_id                 = getIntent().getStringExtra("SUBJECT_ID");
        final String authId                     = getIntent().getStringExtra("AUTH_ID");


        final EditText reportComment              = (EditText)findViewById(R.id.reportComment);
        Button   submitButton                   = (Button)findViewById(R.id.submitReportButton);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                if (reportComment.getText().toString().equals("")) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(view.getContext());
                    dialog.setTitle(getString(R.string.error_field_required));
                    dialog.setMessage(getString(R.string.error_field_required));
                    dialog.show();
                }
                else {

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
                    String currentDateandTime = sdf.format(new Date());

                    Ion.with(view.getContext())
                            .load("PUT", getString(R.string.main_api_url) + "/book/addComment?book_id=" + bookId)
                            .setHeader("School", school)
                            .setHeader("Authentification", authId)
                            .setBodyParameter("subject_id", subject_id)
                            .setBodyParameter("teacher_id", authId)
                            .setBodyParameter("content", reportComment.getText().toString())
                            .setBodyParameter("date", currentDateandTime)
                            .asJsonObject()
                            .setCallback(new FutureCallback<JsonObject>() {
                                @Override
                                public void onCompleted(Exception e, JsonObject result) {
                                    if (e == null) {
                                        System.out.println(result);
                                        if (result.has("message")) {
                                            finish();
                                        } else
                                            Toast.makeText(view.getContext(), "Error", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });

                    System.out.println(getString(R.string.main_api_url) + "/book/note?book_id=" + bookId);

                }
            }
        });
    }
}
