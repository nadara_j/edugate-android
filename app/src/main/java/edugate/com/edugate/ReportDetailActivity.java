package edugate.com.edugate;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import edugate.com.edugate.Object.Mark;
import edugate.com.edugate.Object.Report;

public class ReportDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Report report = new Report();
        report.setContent(getIntent().getStringExtra("REPORT_CONTENT"));
        report.setId(getIntent().getStringExtra("REPORT_ID"));


        final String schoolId = getIntent().getStringExtra("SCHOOL_ID");
        final String authId = getIntent().getStringExtra("AUTH_ID");
        final String bookId = getIntent().getStringExtra("BOOK_ID");


        setContentView(R.layout.activity_report_detail);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        final EditText reportContent = (EditText) findViewById(R.id.reportContent);

        Button edit = (Button) findViewById(R.id.editButton);
        Button delete = (Button) findViewById(R.id.deleteButton);

        reportContent.setText(report.getContent());

        final ReportDetailActivity copy = this;

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ion.with(v.getContext()).load("PUT", getString(R.string.main_api_url) + "/book/updateComment?book_id=" + bookId + "&comment_id=" + report.getId())
                        .setHeader("School", schoolId).setHeader("Authentification", authId)
                        .setBodyParameter("content", reportContent.getText().toString())
                        .asString().setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        System.out.println(result);
                        copy.finish();
                    }
                });
            }
        });



        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ion.with(v.getContext()).load("DELETE", getString(R.string.main_api_url) + "/book/deleteComment?book_id=" + bookId + "&comment_id=" + report.getId())
                        .setHeader("School", schoolId).setHeader("Authentification", authId)
                        .asString().setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        System.out.println(result);
                        copy.finish();
                    }
                });
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;


        }

        return super.onOptionsItemSelected(item);
    }
}
