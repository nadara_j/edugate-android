package edugate.com.edugate.Service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import edugate.com.edugate.LoginActivity;
import edugate.com.edugate.MainActivity;
import edugate.com.edugate.R;

public class NotificationService extends Service {
    private String authID;
    private String schoolID;
    private String accType;
    private String classId;

    public NotificationService() {
    }

    @Override
    public void onCreate() {
        //authID = intent.getStringExtra("authID");
        //schoolID = intent.getStringExtra("schoolID");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null) {

            authID = intent.getStringExtra("authID");
            schoolID = intent.getStringExtra("schoolID");
            classId = intent.getStringExtra("classID");
            accType = intent.getStringExtra("accType");


            System.out.println("on start " + authID);
            System.out.println("on start " + classId);
            System.out.println("on start " + schoolID);


            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    //performe the deskred task
                    System.out.println("notification service");
                    fetchNotifs(authID, schoolID);
                }
            }, 10);
        }

        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void fetchNotifs(final String authID, final String schoolID) {
        System.out.println(authID);
        System.out.println(schoolID);
        Ion.with(this)
                .load(getString(R.string.main_api_url) + "/notification")
                .setHeader("School", schoolID)
                .setHeader("Class", classId)
                .setHeader("Authentification", authID)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        if (e == null) {
                            System.out.println(result);
                            try {
                                JSONObject object = new JSONObject(result);
                                Iterator<?> keys = object.keys();
                                Gson gson = new Gson();



                                while (keys.hasNext()) {
                                    String key = (String) keys.next();
                                    if (object.get(key) instanceof JSONObject) {

                                        if (!((JSONObject) object.get(key)).getBoolean("read") && ((JSONObject) object.get(key)).getString("title") != null
                                                && ((JSONObject) object.get(key)).getString("content") != null) {

                                            Intent intent = new Intent(getBaseContext(), MainActivity.class);
                                            intent.putExtra("SCHOOL_ID", schoolID);
                                            intent.putExtra("AUTH_ID", authID);
                                            intent.putExtra("ACC_TYPE", accType);
                                            intent.putExtra("CLASS_ID", classId);

                                            // use System.currentTimeMillis() to have a unique ID for the pending intent
                                            PendingIntent pIntent = PendingIntent.getActivity(getBaseContext(), (int) System.currentTimeMillis(), intent, PendingIntent.FLAG_UPDATE_CURRENT);

                                            // build notification
                                            // the addAction re-use the same intent to keep the example short
                                            Notification n  = new Notification.Builder(getBaseContext())
                                                    .setContentTitle(((JSONObject) object.get(key)).getString("title"))
                                                    .setContentText(((JSONObject) object.get(key)).getString("content"))
                                                    .setSmallIcon(R.mipmap.ic_launcher)
                                                    .setContentIntent(pIntent)
                                                    .setAutoCancel(true)
                                                    .build();

                                            NotificationManager notificationManager =
                                                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                                            notificationManager.notify((int) System.currentTimeMillis(), n);

                                            Ion.with(getBaseContext())
                                                    .load("PUT", getString(R.string.main_api_url) + "/notification/read?notification_id=" + key)
                                                    .setHeader("School", schoolID)
                                                    .setHeader("Authentification", authID)
                                                    .asString()
                                                    .setCallback(new FutureCallback<String>() {
                                                        @Override
                                                        public void onCompleted(Exception e, String result) {
                                                            System.out.println(result);
                                                        }
                                                    });
                                        }
                                    }

                                }
                            } catch (JSONException ex) {
                                System.out.println("execption");
                            }
                        }
                    }
                });
    }
}
