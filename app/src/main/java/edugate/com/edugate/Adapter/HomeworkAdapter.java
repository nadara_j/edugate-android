package edugate.com.edugate.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import edugate.com.edugate.Object.Homework;
import edugate.com.edugate.Object.Report;
import edugate.com.edugate.R;

/**
 * Created by Jonathan on 14/06/15.
 */

public class HomeworkAdapter extends RecyclerView.Adapter<HomeworkAdapter.ViewHolder> {
    private ArrayList<Homework> mDataset;


    public HomeworkAdapter(ArrayList<Homework> data) {
        mDataset = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v;

        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.homework_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;

        //return null;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");



        final Homework homework = mDataset.get(position);

        Date netDate = (new Date(Long.parseLong(homework.getEndDate())));

        holder.title.setText(homework.getName());
        holder.content.setText(homework.getDesc());
        holder.date.setText(dateFormat.format(netDate));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }




    public void add(int position, Homework item) {
        mDataset.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(Homework item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

   // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView        title;
        public TextView        content;
        public TextView        date;

        public ViewHolder(View v) {
            super(v);

            title       = (TextView) v.findViewById(R.id.homeworkTitle);
            content     = (TextView) v.findViewById(R.id.homeworkContent);
            date        = (TextView) v.findViewById(R.id.homeworkDate);
        }
    }
}
