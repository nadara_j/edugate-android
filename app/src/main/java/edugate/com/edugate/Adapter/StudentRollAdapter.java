package edugate.com.edugate.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import edugate.com.edugate.Object.Student;
import edugate.com.edugate.R;

/**
 * Created by Jonathan on 14/06/15.
 */

public class StudentRollAdapter extends RecyclerView.Adapter<StudentRollAdapter.ViewHolder> {
    private ArrayList<Student> mDataset;


    public StudentRollAdapter(ArrayList<Student> data) {
        mDataset = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v;

        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.mark_student_roll_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;

        //return null;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        final Student student = mDataset.get(position);

        holder.name.setText(student.getName());
        holder.present.setSelected(student.getPresent());

        holder.present.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                student.setPresent(b);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }




    public void add(int position, Student item) {
        mDataset.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(Student item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

   // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView        name;
        public CheckBox        present;

        public ViewHolder(View v) {
            super(v);

            name        = (TextView) v.findViewById(R.id.name);
            present     = (CheckBox) v.findViewById(R.id.enrollStatus);
        }
    }
}
