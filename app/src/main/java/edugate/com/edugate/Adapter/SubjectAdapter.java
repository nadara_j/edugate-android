package edugate.com.edugate.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import edugate.com.edugate.ClassActivity;
import edugate.com.edugate.Object.Student;
import edugate.com.edugate.Object.Subject;
import edugate.com.edugate.R;

/**
 * Created by Jonathan on 14/06/15.
 */

public class SubjectAdapter extends RecyclerView.Adapter<SubjectAdapter.ViewHolder> {
    private ArrayList<Subject> mDataset;


    public SubjectAdapter(ArrayList<Subject> data) {
        mDataset = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v;

        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.mark_subject_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;

        //return null;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        final Subject subject = mDataset.get(position);

        holder.name.setText(subject.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = ((Activity) v.getContext()).getIntent();
                String acc_type = intent.getExtras().getString("ACC_TYPE");
                String auth_id = intent.getExtras().getString("AUTH_ID");
                String school_id = intent.getExtras().getString("SCHOOL_ID");
                String transaction = intent.getExtras().getString("TRANSACTION_TYPE");

                intent = new Intent(v.getContext(), ClassActivity.class);
                intent.putExtra("AUTH_ID", auth_id);
                intent.putExtra("SCHOOL_ID", school_id);
                intent.putExtra("ACC_TYPE", acc_type);
                intent.putExtra("SUBJECT_ID", subject.getId());

                intent.putExtra("TRANSACTION_TYPE", transaction);

                Log.d("subject adapter", intent.getExtras().getString("TRANSACTION_TYPE"));
                v.getContext().startActivity(intent);
            }
        });
    }




    public void add(int position, Subject item) {
        mDataset.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(Subject item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

   // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView        name;

        public ViewHolder(View v) {
            super(v);

            name       = (TextView) v.findViewById(R.id.name);
        }
    }
}
