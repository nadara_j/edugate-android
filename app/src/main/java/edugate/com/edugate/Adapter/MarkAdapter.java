package edugate.com.edugate.Adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import edugate.com.edugate.MarkDetailActivity;
import edugate.com.edugate.Object.Mark;
import edugate.com.edugate.R;

/**
 * Created by Jonathan on 14/06/15.
 */

public class MarkAdapter extends RecyclerView.Adapter<MarkAdapter.ViewHolder> {
    private ArrayList<Mark> mDataset;

    private String schoolId;

    public String getAccType() {
        return accType;
    }

    public void setAccType(String accType) {
        this.accType = accType;
    }

    private String accType;

    private String authToken;

    public MarkAdapter(ArrayList<Mark> data) {
        mDataset = data;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }


    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v;

        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.mark_mark_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;

        //return null;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        final Mark mark = mDataset.get(position);

        holder.name.setText(mark.getName());
        holder.mark.setText(mark.getMark());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getAccType().equals("Teacher"))
                {
                    Intent intent = new Intent(v.getContext(), MarkDetailActivity.class);
                    intent.putExtra("MARK_VALUE", mark.getMark());
                    intent.putExtra("MARK_NAME", mark.getName());
                    intent.putExtra("MARK_ID", mark.getId());
                    intent.putExtra("MARK_COEFF", mark.getCoefficient());
                    intent.putExtra("MARK_COMMENT", mark.getComment());
                    intent.putExtra("STUDENT_ID", mark.getStudentId());

                    intent.putExtra("SCHOOL_ID", schoolId);
                    intent.putExtra("AUTH_ID", getAuthToken());

                    v.getContext().startActivity(intent);
                }

            }
        });
    }




    public void add(int position, Mark item) {
        mDataset.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(Mark item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

   // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView        name;
        public TextView        mark;

        public ViewHolder(View v) {
            super(v);

            name       = (TextView) v.findViewById(R.id.name);
            mark       = (TextView) v.findViewById(R.id.mark);
        }
    }
}
