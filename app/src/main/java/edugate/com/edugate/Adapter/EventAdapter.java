package edugate.com.edugate.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import edugate.com.edugate.Object.Event;
import edugate.com.edugate.R;
import edugate.com.edugate.StudentsRollActivity;

/**
 * Created by Jonathan on 14/06/15.
 */

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {
    private ArrayList<Event> mDataset;


    public EventAdapter(ArrayList<Event> data) {
        mDataset = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v;

        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.calendar_event_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;

        //return null;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        final Event event = mDataset.get(position);

        holder.start.setText(event.getStart());
        holder.end.setText(event.getEnd());
        holder.who.setText(event.getWho());
        holder.subject.setText(event.getSubject());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!event.isCalendar()) {
                    Intent intent = ((Activity) v.getContext()).getIntent();
                    String acc_type = intent.getExtras().getString("ACC_TYPE");
                    String auth_id = intent.getExtras().getString("AUTH_ID");
                    String school_id = intent.getExtras().getString("SCHOOL_ID");

                    intent = new Intent(v.getContext(), StudentsRollActivity.class);
                    intent.putExtra("AUTH_ID", auth_id);
                    intent.putExtra("SCHOOL_ID", school_id);
                    intent.putExtra("ACC_TYPE", acc_type);
                    intent.putExtra("CLASS_ID", event.getClassId());
                    intent.putExtra("SUBJECT_ID", event.getSubjectId());
                    intent.putExtra("EVENT_START", event.getStart());
                    intent.putExtra("EVENT_END", event.getEnd());
                    intent.putExtra("EVENT_DATE", event.getDate());
                    v.getContext().startActivity(intent);
                }


            }
        });
    }




    public void add(int position, Event item) {
        mDataset.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(Event item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

   // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView        start;
        public TextView        end;
        public TextView        subject;
        public TextView        who;

        public ViewHolder(View v) {
            super(v);

            start       = (TextView) v.findViewById(R.id.start_event);
            end         = (TextView) v.findViewById(R.id.end_event);
            subject         = (TextView) v.findViewById(R.id.subject);
            who         = (TextView) v.findViewById(R.id.who);

    }
    }
}
