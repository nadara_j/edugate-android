package edugate.com.edugate.Adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import edugate.com.edugate.FeedDeatilActivity;
import edugate.com.edugate.MarkDetailActivity;
import edugate.com.edugate.Object.Feed;
import edugate.com.edugate.Object.Mark;
import edugate.com.edugate.R;

/**
 * Created by Jonathan on 14/06/15.
 */

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.ViewHolder> {
    private ArrayList<Feed> mDataset;
    private String schoolId;

    public String getAcc_type() {
        return acc_type;
    }

    public void setAcc_type(String acc_type) {
        this.acc_type = acc_type;
    }

    private String acc_type;

    private String authToken;


    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }


    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public FeedAdapter(ArrayList<Feed> data) {
        mDataset = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v;

        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_feed_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;

        //return null;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        final Feed feed = mDataset.get(position);

        holder.title.setText(feed.getTitle());
        holder.content.setText(feed.getContent());
        holder.date.setText(dateFormat.format(feed.getDate()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getAcc_type().equals("Teacher"))
                {
                    Intent intent = new Intent(v.getContext(), FeedDeatilActivity.class);
                    intent.putExtra("FEED_TITLE", feed.getTitle());
                    intent.putExtra("FEED_CONTENT", feed.getContent());

                    intent.putExtra("FEED_DATE", feed.getDate().toString());
                    intent.putExtra("FEED_ID", feed.getId());
                    intent.putExtra("FEED_CLASS", feed.getClassId());

                    intent.putExtra("SCHOOL_ID", schoolId);
                    intent.putExtra("AUTH_ID", getAuthToken());
                    v.getContext().startActivity(intent);
                }


            }
        });
    }




    public void add(int position, Feed item) {
        mDataset.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(Feed item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

   // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView        title;
        public TextView        content;
        public TextView        date;

        public ViewHolder(View v) {
            super(v);

            title       = (TextView) v.findViewById(R.id.feedTitle);
            content     = (TextView) v.findViewById(R.id.feedContent);
            date        = (TextView) v.findViewById(R.id.feedDate);
        }
    }
}
