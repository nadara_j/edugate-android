package edugate.com.edugate.Adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import edugate.com.edugate.MarkDetailActivity;
import edugate.com.edugate.Object.Feed;
import edugate.com.edugate.Object.Report;
import edugate.com.edugate.R;
import edugate.com.edugate.ReportDetailActivity;

/**
 * Created by Jonathan on 14/06/15.
 */

public class ReportAdapter extends RecyclerView.Adapter<ReportAdapter.ViewHolder> {
    private ArrayList<Report> mDataset;


    public ReportAdapter(ArrayList<Report> data) {
        mDataset = data;
    }

    private String schoolId;

    private String authToken;

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }


    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v;

        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;

        //return null;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        final Report report = mDataset.get(position);

        holder.title.setText(report.getTitle());
        holder.content.setText(report.getContent());
        holder.date.setText(dateFormat.format(report.getDate()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (report.isEditable())
                {
                    Intent intent = new Intent(v.getContext(), ReportDetailActivity.class);
                    intent.putExtra("REPORT_CONTENT", report.getContent());
                    intent.putExtra("REPORT_ID", report.getId());
                    intent.putExtra("BOOK_ID", report.getBookId());

                    intent.putExtra("SCHOOL_ID", schoolId);
                    intent.putExtra("AUTH_ID", getAuthToken());
                    v.getContext().startActivity(intent);
                }
            }
        });
    }




    public void add(int position, Report item) {
        mDataset.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(Report item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

   // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView        title;
        public TextView        content;
        public TextView        date;

        public ViewHolder(View v) {
            super(v);

            title       = (TextView) v.findViewById(R.id.reportTitle);
            content     = (TextView) v.findViewById(R.id.reportContent);
            date        = (TextView) v.findViewById(R.id.reportDate);
        }
    }
}
