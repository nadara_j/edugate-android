package edugate.com.edugate.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import edugate.com.edugate.Object.Report;
import edugate.com.edugate.Object.Ticket;
import edugate.com.edugate.R;
import edugate.com.edugate.TicketDetailActivity;

/**
 * Created by Jonathan on 14/06/15.
 */

public class TicketAdapter extends RecyclerView.Adapter<TicketAdapter.ViewHolder> {
    private ArrayList<Ticket> mDataset;

    private String authId;

    public TicketAdapter(ArrayList<Ticket> data, String _authId) {
        mDataset = data;
        authId = _authId;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v;

        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.ticket_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;

        //return null;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        final Ticket ticket = mDataset.get(position);

        holder.title.setText(ticket.getTitle());

        if (ticket.getSenderId().equals(authId))
            holder.sender.setText(ticket.getReceiverName());
        else
            holder.sender.setText(ticket.getSenderName());
        holder.date.setText(dateFormat.format(ticket.getDate()));


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent  = ((Activity) v.getContext()).getIntent();
                String acc_type = intent.getExtras().getString("ACC_TYPE");
                String auth_id = intent.getExtras().getString("AUTH_ID");
                String school_id = intent.getExtras().getString("SCHOOL_ID");


                intent = new Intent(v.getContext(), TicketDetailActivity.class);

                intent.putExtra("AUTH_ID", auth_id);
                intent.putExtra("SCHOOL_ID", school_id);
                intent.putExtra("ACC_TYPE", acc_type);

                intent.putExtra("TICKET_ID", ticket.getId());


                v.getContext().startActivity(intent);
            }
        });
    }




    public void add(int position, Ticket item) {
        mDataset.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(Report item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

   // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView        title;
        public TextView        sender;
        public TextView        date;

        public ViewHolder(View v) {
            super(v);

            title       = (TextView) v.findViewById(R.id.ticketTitle);
            sender     = (TextView) v.findViewById(R.id.ticketSender);
            date        = (TextView) v.findViewById(R.id.ticketDate);
        }
    }
}
