package edugate.com.edugate.Adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import edugate.com.edugate.Object.Message;
import edugate.com.edugate.Object.Report;
import edugate.com.edugate.R;

/**
 * Created by Jonathan on 14/06/15.
 */

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {
    private ArrayList<Message> mDataset;


    public MessageAdapter(ArrayList<Message> data) {
        mDataset = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v;

        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;

        //return null;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        final Message message = mDataset.get(position);

        holder.sender.setText(message.getSender());
        holder.content.setText(message.getContent());
        System.out.println("gettting content : " + message.getContent());
        holder.date.setText(dateFormat.format(message.getDate()));
        if (message.isOwner() == true)
        {
            holder.relativeLayout.setBackgroundColor(0xFFAAAAAA);
            holder.relativeLayout.setGravity(Gravity.END);


            RelativeLayout.LayoutParams params2 = (RelativeLayout.LayoutParams)holder.content.getLayoutParams();
            params2.addRule(RelativeLayout.ALIGN_PARENT_END);

            holder.content.setLayoutParams(params2); //causes layout update

        }
        else
        {

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }




    public void add(int position, Message item) {
        mDataset.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(Report item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

   // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView        sender;
        public TextView        content;
        public TextView        date;
        public CardView        cardView;
        public RelativeLayout  relativeLayout;

        public ViewHolder(View v) {
            super(v);

            sender       = (TextView) v.findViewById(R.id.messageSender);
            content     = (TextView) v.findViewById(R.id.messageContent);
            date        = (TextView) v.findViewById(R.id.messageDate);
            cardView    = (CardView) v.findViewById(R.id.card_view);
            relativeLayout  = (RelativeLayout) v.findViewById(R.id.relative_layout);
        }
    }
}
