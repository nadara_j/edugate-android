package edugate.com.edugate.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import edugate.com.edugate.AddDetentionActivity;
import edugate.com.edugate.AddTicketsActivity;
import edugate.com.edugate.DetentionActivity;
import edugate.com.edugate.MarksActivity;
import edugate.com.edugate.ReportActivity;
import edugate.com.edugate.Object.Feed;
import edugate.com.edugate.Object.Student;
import edugate.com.edugate.R;
import edugate.com.edugate.StudentsActivity;

/**
 * Created by Jonathan on 14/06/15.
 */

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.ViewHolder> {
    private ArrayList<Student> mDataset;


    public StudentAdapter(ArrayList<Student> data) {
        mDataset = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v;

        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.mark_student_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;

        //return null;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        final Student student = mDataset.get(position);

        holder.name.setText(student.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = ((Activity) v.getContext()).getIntent();
                String acc_type = intent.getExtras().getString("ACC_TYPE");
                String auth_id = intent.getExtras().getString("AUTH_ID");
                String school_id = intent.getExtras().getString("SCHOOL_ID");
                String subject_id = intent.getExtras().getString("SUBJECT_ID");
                String transaction = intent.getExtras().getString("TRANSACTION_TYPE");

                if (transaction.equals("DETENTION"))
                {
                    intent = new Intent(v.getContext(), DetentionActivity.class);
                    intent.putExtra("AUTH_ID", auth_id);
                    intent.putExtra("SCHOOL_ID", school_id);
                    intent.putExtra("ACC_TYPE", acc_type);
                    intent.putExtra("STUDENT_ID", student.getBookId());
                    intent.putExtra("SUBJECT_ID", subject_id);
                    intent.putExtra("TRANSACTION_TYPE", transaction);
                }
                else if (transaction.equals("MARK"))
                {
                    intent = new Intent(v.getContext(), MarksActivity.class);
                    intent.putExtra("AUTH_ID", auth_id);
                    intent.putExtra("SCHOOL_ID", school_id);
                    intent.putExtra("ACC_TYPE", acc_type);
                    intent.putExtra("STUDENT_ID", student.getBookId());
                    intent.putExtra("SUBJECT_ID", subject_id);
                    intent.putExtra("TRANSACTION_TYPE", transaction);
                }
                else if (transaction.equals("REPORT"))
                {
                    intent = new Intent(v.getContext(), ReportActivity.class);
                    intent.putExtra("AUTH_ID", auth_id);
                    intent.putExtra("SCHOOL_ID", school_id);
                    intent.putExtra("ACC_TYPE", acc_type);
                    intent.putExtra("STUDENT_ID", student.getBookId());

                    intent.putExtra("SUBJECT_ID", subject_id);
                    intent.putExtra("TRANSACTION_TYPE", transaction);
                }
                else if (transaction.equals("TICKET"))
                {
                    intent = new Intent(v.getContext(), AddTicketsActivity.class);
                    intent.putExtra("AUTH_ID", auth_id);
                    intent.putExtra("SCHOOL_ID", school_id);
                    intent.putExtra("ACC_TYPE", acc_type);
                    intent.putExtra("STUDENT_ID", student.getId());
                    intent.putExtra("SUBJECT_ID", subject_id);
                    intent.putExtra("TRANSACTION_TYPE", transaction);
                }


                Log.d("subject adapter", intent.getExtras().getString("TRANSACTION_TYPE"));

                v.getContext().startActivity(intent);
            }
        });
    }




    public void add(int position, Student item) {
        mDataset.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(Student item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

   // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView        name;

        public ViewHolder(View v) {
            super(v);

            name       = (TextView) v.findViewById(R.id.name);
        }
    }
}
