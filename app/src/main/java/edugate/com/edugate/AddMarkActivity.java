package edugate.com.edugate;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import edugate.com.edugate.Object.Homework;

public class AddMarkActivity extends AppCompatActivity {
    private ArrayList<String>   subjectArray    = new ArrayList<>();
    private ArrayList<Homework> homeworkArray   = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_mark);

        final Spinner subjectList               = (Spinner)findViewById(R.id.subjectList);

        final String school                     = getIntent().getStringExtra("SCHOOL_ID");
        final String bookId                     = getIntent().getStringExtra("STUDENT_ID");
        final String subject_id                 = getIntent().getStringExtra("SUBJECT_ID");
        final String authId                     = getIntent().getStringExtra("AUTH_ID");

        final Activity backup                   = this;

        Ion.with(this)
                .load(getString(R.string.main_api_url) + "/subject?subject_id=" + subject_id)
                .setHeader("Authentification", authId)
                .setHeader("School", school)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        if (e == null) {
                            System.out.println(result);

                            try {
                                JSONObject object = new JSONObject(result);
                                Iterator<?> keys = object.keys();
                                Gson gson = new Gson();


                                while (keys.hasNext()) {
                                    String key = (String) keys.next();
                                    if (object.get(key) instanceof JSONObject && key.equals("homework_list")) {

                                        object = (JSONObject) (object.get(key));
                                        keys = object.keys();

                                        while (keys.hasNext()) {
                                            key = (String) keys.next();
                                            if (object.get(key) instanceof JSONObject) {
                                                Homework homework = new Homework();
                                                homework.setId(key);
                                                homework.setName(((JSONObject) object.get(key)).get("title").toString());
                                                homework.setDesc(((JSONObject) object.get(key)).get("description").toString());
                                                subjectArray.add(homework.getName());
                                                homeworkArray.add(homework);
                                            }
                                        }

                                        String[] subjectItems = subjectArray.toArray(new String[subjectArray.size()]);
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(backup, android.R.layout.simple_dropdown_item_1line, subjectItems);
                                        subjectList.setAdapter(adapter);
                                        setButtonActionWithHomework();
                                    }
                                }
                            } catch (JSONException error) {
                                System.out.println("json error");
                            }
                        }
                    }
                });



    }

    public void setButtonActionWithHomework() {
        final String school                     = getIntent().getStringExtra("SCHOOL_ID");
        final String bookId                     = getIntent().getStringExtra("STUDENT_ID");
        final String subject_id                 = getIntent().getStringExtra("SUBJECT_ID");
        final String authId                     = getIntent().getStringExtra("AUTH_ID");

        final Spinner subjectList               = (Spinner)findViewById(R.id.subjectList);
        final EditText markValue                = (EditText)findViewById(R.id.markValue);
        final EditText markComment              = (EditText)findViewById(R.id.markComment);
        Button   submitButton                   = (Button)findViewById(R.id.submitMarkButton);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                if (markValue.getText().toString().equals("") || markComment.getText().toString().equals("")) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(view.getContext());
                    dialog.setTitle(getString(R.string.error_field_required));
                    dialog.setMessage(getString(R.string.error_field_required));
                    dialog.show();
                }
                else {
                    System.out.println(homeworkArray.get(subjectList.getSelectedItemPosition()).getId());

                    Ion.with(view.getContext())
                            .load("PUT", getString(R.string.main_api_url) + "/book/addNote?book_id=" + bookId)
                            .setHeader("School", school)
                            .setHeader("Authentification", authId)
                            .setBodyParameter("value", markValue.getText().toString())
                            .setBodyParameter("comment", markComment.getText().toString())
                            .setBodyParameter("homework_id", homeworkArray.get(subjectList.getSelectedItemPosition()).getId())
                            .asJsonObject()
                            .setCallback(new FutureCallback<JsonObject>() {
                                @Override
                                public void onCompleted(Exception e, JsonObject result) {
                                    if (e == null) {
                                        System.out.println(result);
                                        if (result.has("result")) {
                                            if (result.get("result").getAsString().equals("Note added to book")) {
                                                finish();
                                            } else
                                                Toast.makeText(view.getContext(), "Error", Toast.LENGTH_LONG).show();
                                        } else
                                            Toast.makeText(view.getContext(), "Error", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });

                    System.out.println(getString(R.string.main_api_url) + "/book/note?book_id=" + bookId);

                }
            }
        });
    }
}
