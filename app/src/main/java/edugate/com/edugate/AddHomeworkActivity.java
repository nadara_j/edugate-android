package edugate.com.edugate;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

import edugate.com.edugate.Object.Homework;

public class AddHomeworkActivity extends AppCompatActivity {
    private ArrayList<String>   subjectArray    = new ArrayList<>();
    private ArrayList<Homework> homeworkArray   = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_homework);

        final Spinner subjectList               = (Spinner)findViewById(R.id.homeworkTypeList);

        final String school                     = getIntent().getStringExtra("SCHOOL_ID");
        final String bookId                     = getIntent().getStringExtra("STUDENT_ID");
        final String subject_id                 = getIntent().getStringExtra("SUBJECT_ID");
        final String authId                     = getIntent().getStringExtra("AUTH_ID");

        final Activity backup                   = this;



        Ion.with(this)
                .load(getString(R.string.main_api_url) + "/homework_categories")
                        .setHeader("Authentification", authId)
                        .setHeader("School", school)
                        .asString()
                        .setCallback(new FutureCallback<String>() {
                            @Override
                            public void onCompleted(Exception e, String result) {
                                if (e == null) {
                                    System.out.println("testtest");
                                    System.out.println(result);

                                    try {
                                        JSONObject object = new JSONObject(result);
                                        Iterator<?> keys = object.keys();
                                        Gson gson = new Gson();

                                        while (keys.hasNext()) {
                                            String key = (String) keys.next();
                                            System.out.println(key);
                                            if (object.get(key) instanceof JSONObject) {
                                                System.out.println((JSONObject) object.get(key));
                                                Homework homework = new Homework();
                                                homework.setId(key);
                                                homework.setName(((JSONObject) object.get(key)).get("name").toString());
                                                homework.setDesc("");
                                                subjectArray.add(homework.getName());
                                                homeworkArray.add(homework);
                                            }
                                        }

                                        String[] subjectItems = subjectArray.toArray(new String[subjectArray.size()]);
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(backup, android.R.layout.simple_dropdown_item_1line, subjectItems);
                                        subjectList.setAdapter(adapter);
                                        setButtonActionWithHomework();
                                    }
                                    catch (JSONException error) {
                                        System.out.println("json error");
                                    }
                                }
                            }
                        });



    }

    public void setButtonActionWithHomework() {
        final String school                     = getIntent().getStringExtra("SCHOOL_ID");
        final String bookId                     = getIntent().getStringExtra("STUDENT_ID");
        final String subject_id                 = getIntent().getStringExtra("SUBJECT_ID");
        final String authId                     = getIntent().getStringExtra("AUTH_ID");
        final String classId                    = getIntent().getStringExtra("CLASS_ID");


        final Spinner homeworkTypeList          = (Spinner)findViewById(R.id.homeworkTypeList);
        final EditText homeworkTitle            = (EditText)findViewById(R.id.homeworkTitle);
        final EditText homeworkDesc             = (EditText)findViewById(R.id.homeworkDesc);
        final EditText homeworkCoeff            = (EditText)findViewById(R.id.homeworkCoeff);
        final DatePicker startDp                = (DatePicker)findViewById(R.id.datePicker1);
        final DatePicker endDp                  = (DatePicker)findViewById(R.id.datePicker2);


        Button   submitButton                   = (Button)findViewById(R.id.submitHomeworkButton);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                if (homeworkTitle.getText().toString().equals("") || homeworkDesc.getText().toString().equals("")) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(view.getContext());
                    dialog.setTitle(getString(R.string.error_field_required));
                    dialog.setMessage(getString(R.string.error_field_required));
                    dialog.show();
                }
                else {
                    System.out.println(homeworkArray.get(homeworkTypeList.getSelectedItemPosition()).getId());

                    Calendar CalendarStart = new GregorianCalendar(startDp.getYear(), startDp.getMonth(), startDp.getDayOfMonth());
                    Calendar CalendarEnd = new GregorianCalendar(endDp.getYear(), endDp.getMonth(), endDp.getDayOfMonth());



                    String start = String.valueOf(startDp.getYear()) + "-" + String.valueOf(startDp.getMonth() + 1) + "-" +  String.valueOf(startDp.getDayOfMonth());
                    String end = String.valueOf(endDp.getYear()) + "-" + String.valueOf(endDp.getMonth() + 1) + "-" +  String.valueOf(endDp.getDayOfMonth());

                    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        Date startdate = (Date)formatter.parse(start);
                        Date enddate = (Date)formatter.parse(start);


                        System.out.println("start date: " + startdate.getTime());
                        Ion.with(view.getContext())
                                .load("PUT", getString(R.string.main_api_url) + "/subject/addHomework?subject_id=" + subject_id)
                                .setHeader("School", school)
                                .setHeader("Authentification", authId)
                                .setBodyParameter("title", homeworkTitle.getText().toString())
                                .setBodyParameter("description", homeworkDesc.getText().toString())
                                .setBodyParameter("coefficient", homeworkCoeff.getText().toString())
                                .setBodyParameter("start", String.valueOf(startdate.getTime() / 1000))
                                .setBodyParameter("end", String.valueOf(enddate.getTime() / 1000))
                                .setBodyParameter("class_id", classId)
                                .setBodyParameter("homework_category_id", homeworkArray.get(homeworkTypeList.getSelectedItemPosition()).getId())
                                .asJsonObject()
                                .setCallback(new FutureCallback<JsonObject>() {
                                    @Override
                                    public void onCompleted(Exception e, JsonObject result) {
                                        if (e == null) {
                                            System.out.println(result);
                                            if (result.has("result")) {
                                                finish();
                                            } else
                                                Toast.makeText(view.getContext(), "Error", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });
                    }
                    catch (ParseException error) {
                        System.out.println("json error");
                    }




                }
            }
        });
    }
}
