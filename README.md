# README #

This is the official Edugate Android Repo

### Edugate Android ###

* Edugate is a project made to help students and professors have a modern school pattern
* Version : 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Download & Install Android Studio
* Update Android Studio if already installed
* Clone the Repo
* Start contributing !

### Contributors ###

* Nadarajah Jonathan & Lefevre Alexandre